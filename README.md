## Proyecto Inventarios

Basado en Java EE Spring Framework 4.2, Base de datos MySQL con Framework FrontEnd JQuery + BootStrap + DataTable:


1. Importar `dbinventarios.sql` Ubicado en la carpeta `WebContent/WEB-INF/database`.
2. Aperture el proyecto con Eclipse Oxygen y realice las pruebas con JBoss Wildfly.

Desarrollado por : [superahacker]

## @Controller (UsuarioController.java)

Servicio Rest preparado para trabajar metodos `GET` `POST` `PUT` `DELETE`

### Metodo GET
`http://localhost:8080/inventarios/Usuario/Web`

**Response:**
Devuelve la interface viewUsuarios.jsp

### Metodo GET
`http://localhost:8080/inventarios/Usuario/Xls`

**Response:**
Devuelve la lista de usuarios en XLS

### Metodo GET
`http://localhost:8080/inventarios/Usuario/Rest`

**Response:**
{   "message": "2 REGISTRO(S) ENCONTRADO(S)",
    "status": true,
    "data": [
        {
            "usu_codigo": "001",
            "usu_nombre": "ATORRES",
            "usu_descri": "ALEX FERNANDO TORRES CHAHUARA",
            "usu_passwd": "123",
            "usu_email": "d15105@idat.edu.pe",
            "usu_imagen": null,
            "usu_fecreg": "2018-03-22 00:00:00",
            "usu_estcod": 1,
            "usu_estdes": "ACTIVO"
        },
        {
            "usu_codigo": "002",
            "usu_nombre": "CTORRES",
            "usu_descri": "CIELO TORRES ARAUJO",
            "usu_passwd": "123",
            "usu_email": "d15106@idat.edu.pe",
            "usu_imagen": "2018-02-01 00:00:00",
            "usu_fecreg": null,
            "usu_estcod": 1,
            "usu_estdes": "ACTIVO"
        }
    ]
}

### Metodo POST
`http://localhost:8080/inventarios/Usuario/Rest`

**Request:**
{
    "usu_codigo": "003",
    "usu_nombre": "XTORRES",
    "usu_descri": "AXEL TORRES CHAHUARA",
    "usu_passwd": "757",
    "usu_email": "d15107@idat.edu.pe",
    "usu_imagen": null,
    "usu_fecreg": "2018-03-01",
    "usu_estcod": 1,
    "usu_estdes": null
}

**Response:**
{
    "message": "1 REGISTRO INGRESADO",
    "status": true
}

### Metodo PUT
`http://localhost:8080/inventarios/Usuario/Rest`

**Request:**
{
    "usu_codigo": "003",
    "usu_nombre": "XTORRES",
    "usu_descri": "AXEL TORRES CHUCTAYA",
    "usu_passwd": "757",
    "usu_email": "d15107@idat.edu.pe",
    "usu_imagen": null,
    "usu_fecreg": "2018-03-01",
    "usu_estcod": 1,
    "usu_estdes": null
}

**Resonse:**
{
    "message": "1 REGISTRO ACTUALIZADO",
    "status": true
}

### Metodo DELETE
`http://localhost:8080/inventarios/Usuario/Rest/001`

**Response:**
{
    "message": "1 REGISTRO ELIMINADO",
    "status": true
}

[superahacker]: http://superahacker.blogspot.com/