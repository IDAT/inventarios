<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Color Admin | Login Page</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<jsp:include page="jsp/includeBaseCSS.jsp"/>
</head>
<body class="pace-top">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
	    <!-- begin login -->
        <div class="login bg-black animated fadeInDown">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> <b>Color</b> Admin
                    <small>responsive bootstrap 4 admin template</small>
                </div>
                <div class="icon">
                    <i class="fa fa-lock"></i>
                </div>
            </div>
            <!-- end brand -->
            <!-- begin login-content -->
            <div class="login-content">
                <!-- <form action="index.html" method="GET" class="margin-bottom-0"> -->
                    <div class="form-group m-b-20">
                        <input type="text" id="inpUsuario" class="form-control form-control-lg inverse-mode" placeholder="Usuario" required />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="password" id="inpPassword" class="form-control form-control-lg inverse-mode" placeholder="Password" required />
                    </div>
                    <div class="checkbox checkbox-css m-b-20">
                        <input type="checkbox" id="remember_checkbox" /> 
                        <label for="remember_checkbox">
                        	Remember Me
                        </label>
                    </div>
                    <div class="login-buttons">
                        <button id="btnLogin" class="btn btn-success btn-block btn-lg">Sign me in</button>
                    </div>
                <!-- </form> -->
            </div>
            <!-- end login-content -->
        </div>
        <!-- end login -->
        
        <!-- begin theme-panel -->
		<jsp:include page="jsp/includePanel.jsp"/>
        <!-- end theme-panel -->
        
	</div>
	<!-- end page container -->
	
	<jsp:include page="jsp/includeBaseJS.jsp"/>
	
	<script>
		$(document).ready(function() {
			App.init();
		});
		
		
		//--- BEGIN BUTTON LOGIN ----------------------------------------------------
		$('body').on('click', '#btnLogin', function (e) {
			
			var formToJSON =JSON.stringify({
                "USU": $.trim($("#inpUsuario").val()),
			    "PWD": $.trim($("#inpPassword").val())
            });			
						
			$.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/Usuario/Login",
                data: formToJSON,
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (response) {                	
               		if(response.status === true){
               			$(location).attr("href", "${pageContext.request.contextPath}/Usuario/Web");
               		}else{
               			ShowMessage(response.message);
               		}
                },
                error: OnErrorCall
            });
			
		});
		
        function OnErrorCall( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
            	console.log('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
            	console.log('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
            	console.log('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
            	console.log('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
            	console.log('Time out error.');
            } else if (textStatus === 'abort') {
            	console.log('Ajax request aborted.');
            } else {
            	console.log('Uncaught Error: ' + jqXHR.responseText);
           }
        }
        
        
        function ShowMessage(m){
        	//REMOVE NOTIFICACIONES
    		//$.gritter.removeAll();
        	
        	$.gritter.add({
    			title:"Color Admin | Mensaje",
    			text: m,
    			image:"https://seantheme.com/color-admin-v4.0/admin/assets/img/user/user-2.jpg",
    			sticky:!1,
    			time:"",
    			class_name:"my-sticky-class"});
        }
	</script>

</body>
</html>