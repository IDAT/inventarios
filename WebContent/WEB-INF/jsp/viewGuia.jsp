<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
	<head>
	<meta charset="utf-8" />
	<title>Color Admin | Guia</title>
	  	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		
		<jsp:include page="includeBaseCSS.jsp"/>  
		
		<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
		<!-- ================== END PAGE LEVEL STYLE ================== -->
		

		
		<!-- ================== BEGIN BASE JS ================== -->
		<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/pace/pace.min.js"></script>
		<!-- ================== END BASE JS ================== -->

	  	  
	</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	

<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<jsp:include page="includeHeader.jsp"/>
		<!-- end #header -->
		
		<!-- begin #sidebar -->
		<jsp:include page="includeSideBar.jsp"/>
		<!-- end #sidebar -->
		
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item"><a href="javascript:;">Mantenimiento</a></li>
				<li class="breadcrumb-item active">Guia</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Guia <small>Mantenimiento de Guia...</small></h1>
			<!-- end page-header -->
			
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">						
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-info" id="btnPageNewRow"><i class="fa fa-plus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" id="btnPageRefreshRows"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="Xls" class="btn btn-xs btn-icon btn-circle btn-primary"><i class="fa fa-file-excel"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Data Table - Default</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
				
					<!-- INICIO S501CI -->
					<table id="data-table-default" class="table table-striped table-bordered">
						<thead>
							<th></th>							
							<th>Codigo</th>
							<th>Fecha</th>
							<th>Tipo</th>
							<th>Transaccion</th>
							<th>Observacion</th>
							<th>Usuario</th>
							<th>Estado</th>
							<th></th>
						</thead>
						<tbody>
						</tbody>
					</table>
					<!-- FIN S501CI -->
					
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end #content -->

		<!-- #modal-dialog -->
		<div class="modal fade" id="modal-dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="h4ModalTitulo">GUIA</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						    
                        	<form id="formModal">
                        	</form>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-2">Fecha:</label>
									<div class="col-md-4">
										<input type="hidden" id="hddModalCorrel"/>
										<input type="text" class="form-control m-b-5"  id="inpModalFecha" disabled />
									</div>
									
									<label class="col-form-label col-md-2">Tipo:</label>
									<div class="col-md-4">
										<Select class="form-control m-b-5" id="selModalTipo">
											<Option value="-">Seleccione...</Option>
											<Option value="E">ENTRADA </Option>
											<Option value="S">SALIDA </Option> 
										</Select>
									</div>
									
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-2">Transacción:</label>
									<div class="col-md-4">
										<Select class="form-control m-b-5"  id="selModalTransa" >
										</Select>
									</div>
									
									<label class="col-form-label col-md-2">Estado:</label>
									<div class="col-md-4">
										<select class="form-control ui-autocomplete-input" id="selModalEstado">											
										</select>
									</div>									
								</div>								
								<legend class="m-b-15"></legend>
								<div id="divModalAdd" class="form-group row m-b-15">
									<div class="col-md-1">
									<button class="btn btn-md btn-success" id="btnModalSearch" ><i class="fa fa-search"></i></button>
									</div>
									<div class="col-md-5">
										<input type="hidden" id="hddModalCodigo"/>
										<input type="text" class="form-control m-b-5"  id="inpModalDescri" />
										<small class="f-s-12 text-grey-darker">Articulo</small>
									</div>
									
									<div class="col-md-2">
										<input type="hidden" id="hddModalUnidad" disabled />
										<input type="text" class="form-control m-b-5" id="inpModalUnidad" disabled />
										<small class="f-s-12 text-grey-darker">U/M</small>
									</div>
									
									<div class="col-md-2">
										<input type="text" class="form-control m-b-5" id="inpModalCantid" />
										<small class="f-s-12 text-grey-darker">Cantidad.</small>
									</div>
									
									<div class="col-md-1">
									<button class="btn btn-md btn-success" id="btnModalAdd" ><i class="fa fa-plus"></i></button>
									</div>								
								</div>		
								<div class="form-group row m-b-15">
									<table id="data-table-detalle" class="table table-striped table-bordered">
										<thead>
											<th></th>
											<th>Codigo</th>											
											<th>Descripcion</th>
											<th>U/M</th>
											<th>Cantidad</th>
											<th></th>
										</thead>
										<tbody>
										</tbody>
									</table>	
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-12">Observacion</label>
									<div class="col-md-12">
										<textarea  class="form-control m-b-5" rows="2" cols="2" id="txaModalObserv" >
										</textarea>
									</div>
								</div>
							
					</div>
					<div class="modal-footer">
						<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
						<button class="btn btn-success" id="btnModalSave">Guardar</button>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- #modal-dialog -->
		<div class="modal fade" id="modal-dialog-add">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="h4ModalTitulo">ARTICULOS</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<table id="data-table-detalle-add" class="table table-striped table-bordered">
							<thead>
								<th></th>
								<th>Codigo</th>											
								<th>Descripcion</th>
								<th>U/M</th>
								<th>Stock</th>
								<th></th>
							</thead>
							<tbody>
							</tbody>
						</table>	
					</div>
				</div>
			</div>
		</div>
		
		
		
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<jsp:include page="includeBaseJS.jsp"/>
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/js/demo/table-manage-default.demo.min.js"></script>	
	<!-- ================== END PAGE LEVEL JS ================== -->
	<c:url value="/resources/js/functions.js" var="funJS" />
	<script src="${funJS}" type="text/javascript"></script>
	
	<script>
		var listadoEstados = ${listadoEstados};
		var dt;
		var dt1;
		var dt2;
		
		$(document).ready(function() {
			App.init();
			
			// CARGAR COMBOBOX
			var $element = $('#selModalEstado');			
			$.each(listadoEstados, function(i, dato) {			
				var $option = $("<option/>").attr("value", dato.par_argume).text(dato.par_funcion);
				$element.append($option);				
			});
			//-------------------------------------------------------
			
			// CONFIGURAR DATATABLE
			var dt = $("#data-table-default").DataTable({
				responsive:!0,
				columns:[
						{data : null,'defaultContent': '', 'orderable': false, 
							'render': function ( data, type, full, meta ) {
								return  meta.row + 1;
							} 
						},						
						{data : "gic_correl"},
						{data : "gic_fecreg"},
						{data : "gic_tipo"},
						{data : "gic_trades"},
						{data : "gic_observ"},
						{data : "gic_usudes"},
						{data : "gic_estdes"},
						{data : "gic_correl",'orderable': false, 
							'render': function ( data, type, full, meta ) {
								var html = '<button class="btn btn-xs btn-warning" id="btnPageEditRow" data-codigo="' + data + '"><i class="fa fa-eye"></i></button>';
								return html;
							}
						}
						]
			});
			//-------------------------------------------------------
			
			// CONFIGURAR DATATABLE
			dt1 = $("#data-table-detalle").DataTable({
				responsive:!0,				
				oLanguage : {
					sInfo		:	' ',
					sInfoEmpty	:	' ',
					sEmptyTable	:	' ',
					oPaginate : {
						"sFirst":    "&#60;&#60;",
				        "sLast":     "&#62;&#62;",
				        "sNext":     "&#62;",
				        "sPrevious": "&#60;"
					}
				},
				ordering			:	true,
				bScrollAutoCss		:	true,
				bStateSave			:	false,
				bAutoWidth			:	false,
				bScrollCollapse		:	false,
				searching			:	false,
				paging				:	true,
				pagingType			:   "full_numbers",
				iDisplayLength		:	5,
				responsive			:	true,
				bLengthChange		: 	false,
				columns:[
						{data : null,'defaultContent': '', 'orderable': false, 
							'render': function ( data, type, full, meta ) {
								return  meta.row + 1;
							} 
						},						
						{data : "gid_artcod"},
						{data : "gid_artdes"},
						{data : "gid_unddes"},
						{data : "gid_cantid"},
						{data : "gic_correl",'orderable': false, 
							'render': function ( data, type, full, meta ) {
								var html ='<button class="btn btn-xs btn-danger" id="btnModalDeleteRow" data-codigo="' + data + '" ><i class="fa fa-trash-alt"></i></button>';								
								return html;
							}
						}
						]
			});
			//-------------------------------------------------------

			// CONFIGURAR DATATABLE
			var dt2 = $("#data-table-detalle-add").DataTable({
				responsive:!0,				
				columns:[
						{data : null,'defaultContent': '', 'orderable': false, 
							'render': function ( data, type, full, meta ) {
								return  meta.row + 1;
							} 
						},						
						{data : "art_codigo"},
						{data : "art_descri"},
						{data : "art_unddes"},
						{data : "art_stoact"},
						{data : "art_codigo",'orderable': false, 
							'render': function(data, type, row, meta){
								var html ='<button class="btn btn-xs btn-danger" id="btnModalSelect"  data-codigo="' + data + '" data-descri="' + row.art_descri + '" data-unddes="' + row.art_unddes + '"><i class="fa fa-cart-plus"></i></button>';								
								return html;
							}
						}
						]
			});
			//-------------------------------------------------------
			
			//CARGAR DATATABLE
	        LoadDataTableWithAjax();
		});
		
		
		$('#data-table-detalle').on('click', '#btnModalDeleteRow', function (e) {
			dt1.row( $(this).parents('tr') ).remove().draw();
		});
		//-------------------------------------------------------
		
		//--- BEGIN BUTTON GUARDAR GUIA  ----------------------------------------------------
		$('body').on('click', '#btnModalSave', function (e) {
			
			var gc = new Object();
			gc.gic_correl = null;
			gc.gic_fecreg = null;
			gc.gic_tipo   = $('#selModalTipo').val();
			gc.gic_tracod = $('#selModalTransa').val();
			gc.gic_trades = "";
			gc.gic_observ = $('#selModalObserv').val();
			gc.gic_usucod = "USU-001";
			gc.gic_usudes = "";
			gc.gic_detalle= dt1.rows().data().toArray();
			gc.gic_estcod = 1;
			gc.gic_estdes = "";
			
			console.log(JSON.stringify(gc));
			
    			$.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/Guia/Rest",
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data : JSON.stringify(gc),
                async: true,
                success: function (response) {
                		//ADD NOTIFICACION
                		ShowMessage(response.message);
                		
                		if(!response.status){
    						return false;
    					}
                		
                		$('#modal-dialog').modal('hide');
                		
                		//CARGAR DATATABLE
            	        LoadDataTableWithAjax()
                },
                error: OnErrorCall
            });
			
		});
		//-------------------------------------------------------
		
		//--- BEGIN BUTTON AGREGAR ARTICULO  ----------------------------------------------------
		$('body').on('click', '#btnModalAdd', function (e) {			
			dt1.row.add( {
				"gid_correl": null,
				"gid_fecreg": null,
				"gid_tipo"  : $('#selModalTipo').val(),
				"gid_tracod": $('#selModalTransa').val(),
		        "gid_artcod": $('#hddModalCodigo').val(),
		        "gid_artdes": $('#inpModalDescri').val(),
		        "gid_undcod": $('#hddModalUnidad').val(),
		        "gid_unddes": $('#inpModalUnidad').val(),
		        "gid_cantid": $('#inpModalCantid').val(),
		        "gid_estcod": 1
		    } ).draw();
			
			//LIMPIAR TEXTS DIV DETALLE 
			$("#inpModalDescri").val("");
			$("#inpModalUnidad").val("");
			$("#inpModalCantid").val("");
		});
		//-------------------------------------------------------
		
		//--- BEGIN BUTTON SELECT ARTICULO  ----------------------------------------------------
		$('body').on('click', '#btnModalSelect', function (e) {
			$('#hddModalCodigo').val($(this).data('codigo'));
			$('#inpModalDescri').val($(this).data('descri'));
			$('#inpModalUnidad').val($(this).data('unddes'));
			$('#modal-dialog-add').modal('hide');
			$("#inpModalCantid").focus();
		});
		//-------------------------------------------------------
		
		//--- BEGIN BUTTON SEARCH ARTICULO  ----------------------------------------------------
		$('body').on('click', '#btnModalSearch', function (e) {
			$('#data-table-detalle-add').dataTable().fnClearTable();
			$('#data-table-detalle-add').dataTable().fnDraw();
        		$.ajax({
                type: "GET",
                url: "${pageContext.request.contextPath}/Articulo/Rest",
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (response) {
	                	if(response.data.length > 0){
	                		//UPDATE DATATABLES
	                		$('#data-table-detalle-add').dataTable().fnAddData(response.data);
	                	}
                },
                error: OnErrorCall
            });
			
			$('#modal-dialog-add').modal('show');
		});
		//-------------------------------------------------------    	
	
		//--- BEGIN BUTTON REFRESCAR  ----------------------------------------------------
		$('body').on('click', '#btnPageRefreshRows', function (e) {			
			//CARGAR DATATABLE
	        LoadDataTableWithAjax()
			
		});
		//-------------------------------------------------------
		
		//--- BEGIN BUTTON NUEVO ----------------------------------------------------
		$('body').on('click', '#btnPageNewRow', function (e) {
			//REINICIAR FORMULARIO
			$('#formModal')[0].reset();
			$("input[type=hidden]").val('');
			
			//MOSTRAR DIV DE AGREGAR ITEMS
			$("#divModalAdd").show();
			
			//MODIFICAR TITULO DE GUIA
			$("#h4ModalTitulo").text("GUIA ");
			
			//FECHA Y HORA ACTUAL
			var dNow = new Date();
    			var localdate= (dNow.getMonth()+1) + '/' + dNow.getDate() + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes();    
			$('#inpModalFecha').val(localdate);
			
			//DESELECCIONAR
			$("#selModalTipo option[selected]").removeAttr("selected");  
			$('#selModalTipo option[value="-"]').attr("selected", "selected"); //SELECCIONE X DEFAULT
			$("#selModalTransa option").remove();
			$("#selModalEstado option[selected]").removeAttr("selected");
			$('#selModalEstado option[value="1"]').attr("selected", "selected"); //ACTIVA X DEFAULT	
			
			//LIMPIAR TEXTS DIV DETALLE 
			$("#inpModalDescri").val("");
			$("#inpModalUnidad").val("");
			$("#inpModalCantid").val("");
			//LIMPIAR OBSERVACION
			$("#txaModalObserv").val("");
			
			//HABILITAR 
			$("#selModalTipo").removeAttr("disabled")
			$("#selModalTransa").removeAttr("disabled")
			$("#selModalEstado").attr("disabled", "disabled");
			$("#txaModalObserv").removeAttr("disabled")
			
			//LIMPIAR DATATABLES DETALLE
			$('#data-table-detalle').dataTable().fnClearTable();
			$('#data-table-detalle').dataTable().fnDraw();
			
			//MOSTRAR BOTON 
            $("#btnModalSave").show();
            $("#btnModalSave").attr("disabled", "disabled");
            $("#btnModalSearch").attr("disabled", "disabled");
            
			//CARGAR MODAL
            $('#modal-dialog').modal('show');
		});
		//-------------------------------------------------------
		
		//--- BEGIN BUTTON EDITAR ----------------------------------------------------
        $('#data-table-default').on('click', '#btnPageEditRow', function (e) {
        		//REINICIAR FORMULARIO
			$('#formModal')[0].reset();
			$("input[type=hidden]").val('');
			
			//OCULTAR DIV DE AGREGAR ITEMS
			$("#divModalAdd").hide();
			
        		//AJAX PARA OBTENER UNA GUIA
			$.ajax({
                type: "GET",
                url: "Rest/" + $(this).data('codigo'),
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (response) {
	               	//ADD NOTIFICACION
	               	ShowMessage(response.message);
	              			                		
                     if(response.status === true){
                     	//CARGAR DATA
                     	var d = response.data;
                     	$("#h4ModalTitulo").text("GUIA N°: " + $.trim(d.gic_correl));
						$("#hddModalCorrel").val($.trim(d.gic_correl));
						$("#inpModalFecha").val($.trim(d.gic_fecreg));
						$("#selModalTipo option[selected]").removeAttr("selected"); 
						$('#selModalTipo option[value="' + $.trim(d.gic_tipo) + '"]').attr("selected", "selected");	
						
						//CARGA TRANSACCION
						$("#selModalTransa option").remove();
						var $element = $('#selModalTransa');
						var $option = $("<option/>").attr("value", d.gic_tracod).text(d.gic_trades);
						$element.append($option);				
						
						$("#selModalEstado option[selected]").removeAttr("selected");  
						$('#selModalEstado option[value="' + $.trim(d.gic_estcod) + '"]').attr("selected", "selected");	
						$("#txaModalObserv").val($.trim(d.gic_observ));
						
						//DESHABILITAR EDICION
						$("#selModalTipo").attr("disabled", "disabled");
						$("#selModalTransa").attr("disabled", "disabled");
						$("#selModalEstado").attr("disabled", "disabled");
						$("#txaModalObserv").attr("disabled", "disabled");
						
						//UPDATE DATATABLES DETALLE
						$('#data-table-detalle').dataTable().fnClearTable();
						$('#data-table-detalle').dataTable().fnDraw();
	                    $('#data-table-detalle').dataTable().fnAddData(d.gic_detalle);
						
	                    //OCULTAR BOTON 
	                    $("#btnModalSave").hide();
	                    
                     	//CARGAR MODAL
         	            $('#modal-dialog').modal('show');
         	         	
                     }
                },
                error: OnErrorCall
            });
        });
       //-------------------------------------------------------
		//--- BEGIN TEXT AUTOCOMPLETE ----------------------------------------------------		
		$('#inpModalDescri').autocomplete({
			minLength: 2,
	 	    delay : 400,
			source: function (request, response) {
                $.ajax({
               		type: "GET",
                    	url: "${pageContext.request.contextPath}/Articulo/Rest/" + request.term,
                    	success: function (d) {
                    		var realArray = $.makeArray(d.data);
                    		
                         response($.map(realArray, function(v,i){
                                 return {label: v.art_descri,
                                         value: v.art_codigo};
                             
                         }));
                    }
                });
            },
            select:  function(e, ui) {
				var keyvalue = ui.item.value;
				alert("Customer number is " + keyvalue); 

			}
	    });
		
		//--- BEGIN SELECT TIPO ----------------------------------------------------
		$('#selModalTipo').on('change', function (e) {
			//CARGA VACIA
			var datavacia =[];
			fnLoadSelectOptions($('#selModalTransa'),datavacia,true,true);
			$("#btnModalSave").attr("disabled", "disabled");
			$("#btnModalSearch").attr("disabled", "disabled");
			if (this.value ==="-"){ //OPCION NO CONSIDERABLE
		    		return false;
		    }
			
			//AJAX TIPO DE DOCUMENTO
			$.ajax({							
				url : '${pageContext.request.contextPath}/Transaccion/Rest/' + this.value ,
				type : 'GET',
				async : true,
				dataType : "json",
				contentType : "application/json",
				mimeType : "application/json",
				timeout : 5000,
				success : function(response) {
					if(!response.status){
						return false;
					}
					//CARGA TRANSACCION
					fnLoadSelectOptions($('#selModalTransa'),response.data,true,true);
									
				},
				error : OnErrorCall
			});
			
		});
		//-------------------------------------------------------
		
		//--- BEGIN SELECT TRANSACCION ----------------------------------------------------
		$('#selModalTransa').on('change', function (e) {
			$("#btnModalSearch").attr("disabled", "disabled");
			$("#btnModalSave").attr("disabled", "disabled");
			if (this.value ==="-"){ //OPCION NO CONSIDERABLE
		    		return false;
		    }
			$("#btnModalSearch").removeAttr("disabled")
			$("#btnModalSave").removeAttr("disabled")
		});
		//-------------------------------------------------------
		
		//--- BEGIN SELECT TRANSACCION CARGAR LIST ----------------------------------------------------
		function fnLoadSelectOptions(ObjectSelect, ObjectListArray, bolOptionPreview, bolValueConcat){
			var $element = ObjectSelect;
			$element.empty();
			//AGREGAR VALOR NEUTRO
			if(bolOptionPreview == true){
				var $option = $("<option/>").attr("value", "-").text("Seleccione...");
				$element.append($option); 
			}
			//AGREGAR VALORES DE LA LISTA
			$.each(ObjectListArray, function(i, dato) {				
				var $option = $("<option/>").attr("value", dato.tra_codigo).text((bolValueConcat ==true ? dato.tra_codigo + ' ' + dato.tra_descri : dato.tra_descri));
				$element.append($option);					
			});
			$element.prop('disabled', false);
		}
		//-------------------------------------------------------
		
        function LoadDataTableWithAjax(){
        		$('#data-table-default').dataTable().fnClearTable();
			$('#data-table-default').dataTable().fnDraw();
        		$.ajax({
                type: "GET",
                url: "${pageContext.request.contextPath}/Guia/Rest",
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (response) {                                            
	                	//ADD NOTIFICACION
	                	ShowMessage(response.message);
	                	
	                	if(response.data.length > 0){
	                		//UPDATE DATATABLES
	                		$('#data-table-default').dataTable().fnAddData(response.data);
	                	}
                },
                error: OnErrorCall
            });
        }
      //-------------------------------------------------------
	</script>
  </body> 
</html>