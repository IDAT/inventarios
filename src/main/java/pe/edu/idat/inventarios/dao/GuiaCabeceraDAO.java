package pe.edu.idat.inventarios.dao;

import java.util.List;
import java.util.Map;

import pe.edu.idat.inventarios.bean.GuiaCabeceraBean;


public interface GuiaCabeceraDAO {
	
	public GuiaCabeceraBean selGuiaCabeceraByCorrel(String gicCorrel) throws Exception;
    
    public List<GuiaCabeceraBean> selGuiaCabecera() throws Exception;
    
    public Map<String, Object> insGuiaCabecera(GuiaCabeceraBean guiaCabecera)throws Exception;

}