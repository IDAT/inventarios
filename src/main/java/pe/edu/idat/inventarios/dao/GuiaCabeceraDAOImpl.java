package pe.edu.idat.inventarios.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import pe.edu.idat.inventarios.bean.GuiaCabeceraBean;

public class GuiaCabeceraDAOImpl implements GuiaCabeceraDAO{
	// DECLARACIONES DAO
	//static Log log = LogFactory.getLog(GuiaCabeceraDAOImpl.class.getName());
	final static Logger log = Logger.getLogger(GuiaCabeceraDAOImpl.class);
	private JdbcTemplate jdbcTemplate;

    private GuiaCabeceraBean guiaCabeceraBean = null;
	    
	// JDBCTEMPLATE
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    
	@Override
	public GuiaCabeceraBean selGuiaCabeceraByCorrel(String gicCorrel) throws Exception {
		
		if (log.isDebugEnabled()) log.debug("Inicio - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
       
        String SQL ="call prcGuiaCabeceraSelectByCorrel(?)";
        try {		
        	guiaCabeceraBean = jdbcTemplate.query(SQL, new Object[] {gicCorrel} ,new ResultSetExtractor<GuiaCabeceraBean>(){
	            @Override
	            public GuiaCabeceraBean extractData(ResultSet rs) throws SQLException{	            	
	            	GuiaCabeceraBean c = null;
					if(rs.next()){
							c = new GuiaCabeceraBean();
					    	c.setGic_correl(rs.getString("gic_correl"));
					    	c.setGic_fecreg(rs.getTimestamp("gic_fecreg"));
					    	c.setGic_tipo(rs.getString("gic_tipo"));
					    	c.setGic_tracod(rs.getString("gic_tracod"));
					    	c.setGic_trades(rs.getString("tra_descri"));
					    	c.setGic_observ(rs.getString("gic_observ"));
					    	c.setGic_usucod(rs.getString("gic_usucod"));
					    	c.setGic_usudes(rs.getString("usu_nombre"));
					    	c.setGic_observ(rs.getString("gic_observ"));
					    	c.setGic_estcod(rs.getInt("gic_estcod"));
					    	c.setGic_estdes(rs.getString("par_funcion"));					    
					}
					return c;
	            }
	        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
		}
			
		return guiaCabeceraBean;
	}

	@Override
	public ArrayList<GuiaCabeceraBean> selGuiaCabecera() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - GuiaCabeceraDAOImpl.selGuiaCabecera");
		
		ArrayList<GuiaCabeceraBean> lstGuiaCabecera = null;
		String SQL ="call prcGuiaCabeceraSelect";
		
		try {		
			lstGuiaCabecera = jdbcTemplate.query(SQL, new ResultSetExtractor<ArrayList<GuiaCabeceraBean>>(){
	            @Override
	            public ArrayList<GuiaCabeceraBean> extractData(ResultSet rs) throws SQLException{
	            		ArrayList<GuiaCabeceraBean> list = new ArrayList<>();
					while (rs.next()){
					    	GuiaCabeceraBean c = new GuiaCabeceraBean();
					    	c.setGic_correl(rs.getString("gic_correl"));
					    	c.setGic_fecreg(rs.getTimestamp("gic_fecreg"));
					    	c.setGic_tipo(rs.getString("gic_tipo"));
					    	c.setGic_tracod(rs.getString("gic_tracod"));
					    	c.setGic_trades(rs.getString("tra_descri"));
					    	c.setGic_observ(rs.getString("gic_observ"));
					    	c.setGic_usucod(rs.getString("gic_usucod"));
					    	c.setGic_usudes(rs.getString("usu_nombre"));
					    	c.setGic_estcod(rs.getInt("gic_estcod"));
					    	c.setGic_estdes(rs.getString("par_funcion"));
					    list.add(c);
					}
					return list;
	            }
	        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - GuiaCabeceraDAOImpl.selGuiaCabecera");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - GuiaCabeceraDAOImpl.selGuiaCabecera");
		}
	    return lstGuiaCabecera;
	}

	@Override
	public Map<String, Object> insGuiaCabecera(GuiaCabeceraBean guiaCabecera) throws Exception {
        if (log.isDebugEnabled()) log.debug("Inicio - GuiaCabeceraDAOImpl.insGuiaCabecera");         
        Map<String, Object> out= new HashMap<String,Object>();  
		try {			
		
			//USING SimpleJdbcCall
			//-------------------------------------------------------------------------------------
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource())
					//.withSchemaName(schema)
				    //.withCatalogName(package)
					.withProcedureName("prcGuiaCabeceraInsert");
			
		 	MapSqlParameterSource params = new MapSqlParameterSource();
		 	params.addValue("in_tipo", guiaCabecera.getGic_tipo());
            params.addValue("in_tracod", guiaCabecera.getGic_tracod());
            params.addValue("in_observ", guiaCabecera.getGic_observ());           
            params.addValue("in_usucod", guiaCabecera.getGic_usucod());
            params.addValue("in_estcod", guiaCabecera.getGic_estcod());
            
		    out = jdbcCall.execute(params);
		    
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - GuiaCabeceraDAOImpl.insGuiaCabecera");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - GuiaCabeceraDAOImpl.insGuiaCabecera");
		}
		return  out;
	}

	
}
