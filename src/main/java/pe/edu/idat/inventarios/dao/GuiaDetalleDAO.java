package pe.edu.idat.inventarios.dao;

import java.util.ArrayList;
import pe.edu.idat.inventarios.bean.GuiaDetalleBean;

public interface GuiaDetalleDAO {
	
	public ArrayList<GuiaDetalleBean> selGuiaDetalleByCorrel(String correlativo) throws Exception;	
	public boolean insGuiaDetalle(GuiaDetalleBean d)throws Exception;
}
