package pe.edu.idat.inventarios.dao;

import java.util.List;
import java.util.Map;

import pe.edu.idat.inventarios.bean.ParametroBean;

public interface ParametroDAO {
	public List<ParametroBean> selParametro(Map<String, Object> parametros) throws Exception;
}
