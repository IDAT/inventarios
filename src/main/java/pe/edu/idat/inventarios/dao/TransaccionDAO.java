package pe.edu.idat.inventarios.dao;

import java.util.List;

import pe.edu.idat.inventarios.bean.TransaccionBean;

public interface TransaccionDAO {
	public List<TransaccionBean> selTransacciones(String c) throws Exception;
}
