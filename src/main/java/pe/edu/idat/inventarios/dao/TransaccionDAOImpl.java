package pe.edu.idat.inventarios.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.inventarios.bean.TransaccionBean;

public class TransaccionDAOImpl implements TransaccionDAO{
		// DECLARACIONES DAO
		static Log log = LogFactory.getLog(UsuarioDAOImpl.class.getName());	
		private JdbcTemplate jdbcTemplate;
		
		// JDBCTEMPLATE
	    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
	        this.jdbcTemplate = jdbcTemplate;
	    }
			
		@Override
		public List<TransaccionBean> selTransacciones(String c) throws Exception {
			List<TransaccionBean> lst;
			
			if (log.isDebugEnabled()) log.debug("Inicio - TransaccionDAOImpl.selTransacciones");
	       
	        String SQL ="call prcTransacionSelectByTipo(?)";
	        try {		
	        	lst = jdbcTemplate.query(SQL, new Object[] {c} ,new ResultSetExtractor<List<TransaccionBean>>(){
		            @Override
		            public List<TransaccionBean> extractData(ResultSet rs) throws SQLException{	            	
		            	List<TransaccionBean> l =  new ArrayList<>();
						while(rs.next()){
							TransaccionBean a = new TransaccionBean();
							a.setTra_codigo(rs.getString("tra_codigo"));
							a.setTra_tipo(rs.getString("tra_tipo"));
							a.setTra_descri(rs.getString("tra_descri"));
							a.setTra_fecreg(rs.getTimestamp("tra_fecreg"));
							a.setTra_estcod(rs.getInt("tra_estcod"));
						    	l.add(a);
						}
						return l;
		            }
		        });
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
			}
				
			return lst;
		}

	


}
