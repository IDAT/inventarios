package pe.edu.idat.inventarios.dao;

import java.util.List;
import pe.edu.idat.inventarios.bean.UsuarioBean;

public interface UsuarioDAO {
	 
    public UsuarioBean selUsuarioByCodigo(String usuCodigo) throws Exception;
     
    public List<UsuarioBean> selUsuario() throws Exception;
    
    public boolean insUsuario(UsuarioBean usuario)throws Exception;
    
    public boolean updUsuario(UsuarioBean usuario)throws Exception;
    
    public boolean delUsuario(String usuCodigo)throws Exception; 
    
    public UsuarioBean selUsuarioByCodigoAndPassword(String usuCodigo,String usuPassword) throws Exception;
}
