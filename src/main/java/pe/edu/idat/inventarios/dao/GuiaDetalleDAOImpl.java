package pe.edu.idat.inventarios.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;


import pe.edu.idat.inventarios.bean.GuiaDetalleBean;

public class GuiaDetalleDAOImpl implements GuiaDetalleDAO{
	// DECLARACIONES DAO
	//static Log log = LogFactory.getLog(GuiaCabeceraDAOImpl.class.getName());
	final static Logger log = Logger.getLogger(GuiaCabeceraDAOImpl.class);
	
	private JdbcTemplate jdbcTemplate;
    private int rows;
	    
	// JDBCTEMPLATE
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    
	@Override
	public ArrayList<GuiaDetalleBean> selGuiaDetalleByCorrel(String correlativo) throws Exception {

		if (log.isDebugEnabled()) log.debug("Inicio - GuiaDetalleDAOImpl.selGuiaDetalleByCorrel");
		
		ArrayList<GuiaDetalleBean> lstGuiaDetalle = null;
		String SQL ="call prcGuiaDetalleSelectByCorrel(?)";
		
		try {		
			lstGuiaDetalle = jdbcTemplate.query(SQL, new Object[] { correlativo } , new ResultSetExtractor<ArrayList<GuiaDetalleBean>>(){
	            @Override
	            public ArrayList<GuiaDetalleBean> extractData(ResultSet rs) throws SQLException{
	            		ArrayList<GuiaDetalleBean> list = new ArrayList<>();
					while (rs.next()){
							GuiaDetalleBean d = new GuiaDetalleBean();
					    	d.setGid_correl(rs.getString("gid_correl"));
					    	d.setGid_fecreg(rs.getTimestamp("gid_fecreg"));
					    	d.setGid_tipo(rs.getString("gid_tipo"));
					    	d.setGid_tracod(rs.getString("gid_tracod"));
					    	d.setGid_artcod(rs.getString("gid_artcod"));					    	
					    	d.setGid_artdes(rs.getString("art_descri"));
					    	d.setGid_unddes(rs.getString("und_abrevi"));
					    	d.setGid_cantid(rs.getDouble("gid_cantid"));
					    	d.setGid_estcod(rs.getInt("gid_estcod"));
					    list.add(d);
					}
					return list;
	            }
	        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - GuiaDetalleDAOImpl.selGuiaDetalleByCorrel");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - GuiaDetalleDAOImpl.selGuiaDetalleByCorrel");
		}
	    return lstGuiaDetalle;
	}

	@Override
	public boolean insGuiaDetalle(GuiaDetalleBean d) throws Exception {
        if (log.isDebugEnabled()) log.debug("Inicio - GuiaDetalleDAOImpl.insGuiaDetalle");
        String SQL ="call prcGuiaDetalleInsert(?,?,?,?,?,?)";
		try {			
			rows = jdbcTemplate.update(SQL.toString(), new Object[] {
					d.getGid_correl(),
					d.getGid_tipo(),
					d.getGid_tracod(),
					d.getGid_artcod(),
					d.getGid_cantid(),
					d.getGid_estcod()});
			
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - GuiaDetalleDAOImpl.insGuiaDetalle");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - GuiaDetalleDAOImpl.insGuiaDetalle");
		}
		return  rows ==1;		
	}

}
