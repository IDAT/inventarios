package pe.edu.idat.inventarios.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.inventarios.bean.ArticuloBean;

public class ArticuloDAOImpl implements ArticuloDAO{
	// DECLARACIONES DAO
	static Log log = LogFactory.getLog(UsuarioDAOImpl.class.getName());	
	private JdbcTemplate jdbcTemplate;
	
	// JDBCTEMPLATE
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
		
	@Override
	public List<ArticuloBean> selArticulosByDescri(String c) throws Exception {
		List<ArticuloBean> lst;
		
		if (log.isDebugEnabled()) log.debug("Inicio - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
       
        String SQL ="call prcArticuloSelectByDescri(?)";
        try {		
        	lst = jdbcTemplate.query(SQL, new Object[] {c} ,new ResultSetExtractor<List<ArticuloBean>>(){
	            @Override
	            public List<ArticuloBean> extractData(ResultSet rs) throws SQLException{	            	
	            	List<ArticuloBean> l =  new ArrayList<>();
					while(rs.next()){
						ArticuloBean a = new ArticuloBean();
						a.setArt_codigo(rs.getString("art_codigo"));
						a.setArt_descri(rs.getString("art_descri"));
						a.setArt_undcod(rs.getString("art_undcod"));
						a.setArt_fecreg(rs.getTimestamp("art_fecreg"));
						a.setArt_estcod(rs.getInt("art_estcod"));
						
					    l.add(a);
					}
					return l;
	            }
	        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
		}
			
		return lst;
	}

	@Override
	public List<ArticuloBean> selArticulos() throws Exception {
		List<ArticuloBean> lst;
		
		if (log.isDebugEnabled()) log.debug("Inicio - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
       
        String SQL ="call prcArticuloSelect()";
        try {		
        	lst = jdbcTemplate.query(SQL, new ResultSetExtractor<List<ArticuloBean>>(){
	            @Override
	            public List<ArticuloBean> extractData(ResultSet rs) throws SQLException{	            	
	            	List<ArticuloBean> l =  new ArrayList<>();
					while(rs.next()){
						ArticuloBean a = new ArticuloBean();
						a.setArt_codigo(rs.getString("art_codigo"));
						a.setArt_descri(rs.getString("art_descri"));
						a.setArt_undcod(rs.getString("art_undcod"));
						a.setArt_unddes(rs.getString("und_descri"));
						a.setArt_stoact(rs.getDouble("art_stoact"));
						a.setArt_fecreg(rs.getTimestamp("art_fecreg"));
						a.setArt_estcod(rs.getInt("art_estcod"));
						
					    l.add(a);
					}
					return l;
	            }
	        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - GuiaCabeceraDAOImpl.selGuiaCabeceraByCorrel");
		}
			
		return lst;
	}

}
