package pe.edu.idat.inventarios.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import pe.edu.idat.inventarios.dao.UsuarioDAOImpl;
import pe.edu.idat.inventarios.bean.UsuarioBean;
import pe.edu.idat.inventarios.controller.UsuarioController;

public class UsuarioDAOImpl implements UsuarioDAO{
	// DECLARACIONES DAO
	//static Log log = LogFactory.getLog(UsuarioDAOImpl.class.getName());
	final static Logger log = Logger.getLogger(UsuarioController.class);
	private JdbcTemplate jdbcTemplate;
    private int rows;
    private UsuarioBean usuarioBean = null;
    
	// JDBCTEMPLATE
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
        
    @SuppressWarnings({ "unchecked", "rawtypes" })
	// LISTAR X CODIGO
	@Override
	public UsuarioBean selUsuarioByCodigo(String usuCodigo)  throws Exception{
        if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selUsuarioByCodigo");
        
        String SQL = "SELECT * FROM tbusuarios WHERE usu_codigo = ? ";        
		try {
			usuarioBean = (UsuarioBean) jdbcTemplate.queryForObject(SQL, new Object[] { usuCodigo }, new RowMapper(){
			            @Override
			            public UsuarioBean mapRow(ResultSet rs, int rowNum) throws SQLException
			            {
			            	UsuarioBean u = new UsuarioBean();
		                	u.setUsu_codigo(rs.getString("usu_codigo"));
		                	u.setUsu_nombre(rs.getString("usu_nombre"));
		                	u.setUsu_descri(rs.getString("usu_descri"));
		                	u.setUsu_passwd(rs.getString("usu_passwd"));
		                	u.setUsu_email(rs.getString("usu_email"));
		                	u.setUsu_imagen(rs.getString("usu_imagen"));
		                	u.setUsu_fecreg(rs.getTimestamp("usu_fecreg"));
		                	u.setUsu_estcod(rs.getInt("usu_estcod"));
			                return u;
			            }
			        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.selUsuarioByCodigo");
			log.error(ex, ex);
			throw ex;	
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.selUsuarioByCodigo");
		}
		
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selectUsuarioByCodigo");
		
		return usuarioBean;
	}
	
	// LISTAR 
    @Override
	public List<UsuarioBean> selUsuario() throws Exception{		
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selectUsuario");
		
		List<UsuarioBean> lstUsuarios = null;
		/*
		 * V.1 TSQL
		String SQL ="SELECT * FROM tbusuarios";
		*/
		
		String strsql ="call prcUsuarioSelect";
		
		try {		
			lstUsuarios = jdbcTemplate.query(strsql, new ResultSetExtractor<List<UsuarioBean>>(){
	            @Override
	            public List<UsuarioBean> extractData(ResultSet rs) throws SQLException
	            {
	            	List<UsuarioBean> list = new ArrayList<>();
	                while (rs.next())
	                {
	                	UsuarioBean u = new UsuarioBean();
	                	u.setUsu_codigo(rs.getString("usu_codigo"));
	                	u.setUsu_nombre(rs.getString("usu_nombre"));
	                	u.setUsu_descri(rs.getString("usu_descri"));
	                	u.setUsu_passwd(rs.getString("usu_passwd"));
	                	u.setUsu_email(rs.getString("usu_email"));
	                	u.setUsu_imagen(rs.getString("usu_imagen"));
	                	u.setUsu_fecreg(rs.getTimestamp("usu_fecreg"));
	                	u.setUsu_estcod(rs.getInt("usu_estcod"));		                	
	                    list.add(u);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.selectUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.selectUsuario");
		}
	    return lstUsuarios;
	}

	// INSERTAR
	@Override
	public boolean insUsuario(UsuarioBean usuario) throws Exception {		
        if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.insUsuario");
        /*
         * V.1 TSQL
        StringBuilder stbsql = new StringBuilder(); 
        SQL.append("INSERT INTO tbusuarios ");
        SQL.append("(usu_codigo, usu_nombre, usu_passwd, usu_descri, usu_email, usu_fecreg, usu_imagen, usu_estcod) ");
        SQL.append("VALUES(?,?,?,?,?,?,?,?)");
        */
        
        String strsql ="call prcUsuarioInsert(?,?,?,?,?)";
		try {			
			rows = jdbcTemplate.update(strsql, new Object[] {
					 usuario.getUsu_nombre(),
					 usuario.getUsu_passwd(),
					 usuario.getUsu_descri(),
					 usuario.getUsu_email(),
					 usuario.getUsu_estcod() });
			
			
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.insUsuario");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.insUsuario");
		}
		return  rows ==1;
	}
	
	//ACTUALIZAR
	@Override
	public boolean updUsuario(UsuarioBean usuario) throws Exception {		
        if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.updUsuario");		
		String strsql = "UPDATE tbusuarios SET usu_nombre =?, usu_descri = ?, usu_passwd = ?, usu_estcod = ?  WHERE usu_codigo = ?";
		try {
			rows = jdbcTemplate.update(strsql, new Object[] {usuario.getUsu_nombre(), 
					usuario.getUsu_descri(),
					 usuario.getUsu_passwd(), 
					 usuario.getUsu_estcod(), 
					 usuario.getUsu_codigo() });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.updUsuario");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.updUsuario");
		}
		return  rows ==1;
	}
	
	// ELIMINAR
	@Override
	public boolean delUsuario(String usuCodigo) throws Exception {		
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.delUsuario");		
		String strsql = "DELETE FROM tbusuarios WHERE usu_codigo = ? ";		
		try {			
			rows = jdbcTemplate.update(strsql, new Object[] { usuCodigo });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.delUsuario");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.delUsuario");
		}	     
		return rows == 1;
	}

	@Override
	public UsuarioBean selUsuarioByCodigoAndPassword(String usuNombre,String usuPassword) throws Exception {		
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selUsuarioByCodigoAndPassword");		
		
		try {
			//USING SimpleJdbcCall
			//-------------------------------------------------------------------------------------
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource()).withProcedureName("prcUsuarioLogin");
			 	MapSqlParameterSource params = new MapSqlParameterSource();
			 	params.addValue("in_nombre", usuNombre);
	            params.addValue("in_passwd", usuPassword);
	            
			    Map<String, Object> out = jdbcCall.execute(params);
			    
			    if(out.get("out_codigo") != null){				   
			    	usuarioBean= new UsuarioBean();
				    usuarioBean.setUsu_codigo((String) out.get("out_codigo"));
				    usuarioBean.setUsu_nombre(usuNombre);
				    usuarioBean.setUsu_passwd(usuPassword);
				    usuarioBean.setUsu_descri((String) out.get("out_descri"));
				    usuarioBean.setUsu_email((String) out.get("out_email"));			    
				    usuarioBean.setUsu_fecreg(((Date) out.get("out_fecreg")));
				    usuarioBean.setUsu_estcod((Integer) out.get("out_estcod"));
			    }
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.selUsuarioByCodigoAndPassword");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.selUsuarioByCodigoAndPassword");
		}	     
		return usuarioBean;
	}
	
}
