package pe.edu.idat.inventarios.dao;

import java.util.List;

import pe.edu.idat.inventarios.bean.ArticuloBean;

public interface ArticuloDAO {
	public List<ArticuloBean> selArticulosByDescri(String c) throws Exception;
	public List<ArticuloBean> selArticulos() throws Exception;
}
