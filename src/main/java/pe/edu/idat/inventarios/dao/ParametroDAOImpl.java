package pe.edu.idat.inventarios.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.inventarios.bean.ParametroBean;
import pe.edu.idat.inventarios.controller.UsuarioController;


public class ParametroDAOImpl implements ParametroDAO {
	// DECLARACIONES DAO
	//static Log log = LogFactory.getLog(UsuarioDAOImpl.class.getName());
	final static Logger log = Logger.getLogger(UsuarioController.class);
	private JdbcTemplate jdbcTemplate;
	
	
	// JDBCTEMPLATE <-> recursos-humanos-descanso-servlet.xml
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    
	@Override
	public List<ParametroBean> selParametro(Map<String, Object> parametros) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ParametroDAOImpl.selectParametro");
		
		List<ParametroBean> lstParametros = null;
		String sql ="SELECT * FROM tbinvparametros WHERE par_codigo = ? AND par_tipcod=? ";
		
		try {
		
			 lstParametros = jdbcTemplate.query(sql, 
					new Object[] { parametros.get("codigo").toString() , parametros.get("argumento").toString() } , 
					new ResultSetExtractor<List<ParametroBean>>(){
		
	            @Override
	            public List<ParametroBean> extractData(ResultSet rs) throws SQLException
	            {
	            	List<ParametroBean> list = new ArrayList<>();
	                while (rs.next())
	                {
	                	ParametroBean p = new ParametroBean();
	                	p.setPar_codigo(rs.getString("par_codigo"));
	                	p.setPar_tipcod(rs.getString("par_tipcod"));
	                	p.setPar_argume(rs.getString("par_argume"));
	                	p.setPar_funcion(rs.getString("par_funcion"));
	                	p.setPar_usucod(rs.getString("par_usucod"));	                	
	                	p.setPar_fecreg(rs.getTimestamp("par_fecreg"));	                		                	
	                    list.add(p);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - ParametroDAOImpl.selectParametro");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ParametroDAOImpl.selectParametro");
		}
	    return lstParametros;
	}

}
