package pe.edu.idat.inventarios.service;

import java.util.List;
import java.util.Map;

import pe.edu.idat.inventarios.bean.ParametroBean;

public interface ParametroService {
	public List<ParametroBean> listarParametros(Map<String, Object> parametros) throws Exception;

}
