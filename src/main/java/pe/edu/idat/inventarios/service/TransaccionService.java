package pe.edu.idat.inventarios.service;

import java.util.List;

import pe.edu.idat.inventarios.bean.TransaccionBean;

public interface TransaccionService {
	public List<TransaccionBean> listarTransacciones(String tipo) throws Exception;

}
