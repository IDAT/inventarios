package pe.edu.idat.inventarios.service;

import java.util.List;

import pe.edu.idat.inventarios.bean.ArticuloBean;

public interface ArticuloService {
	public List<ArticuloBean> listarArticulos() throws Exception;
	public List<ArticuloBean> listarArticulosxDescri(String c) throws Exception;	
}
