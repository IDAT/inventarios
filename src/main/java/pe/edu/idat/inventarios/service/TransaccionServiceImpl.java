package pe.edu.idat.inventarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.inventarios.bean.TransaccionBean;
import pe.edu.idat.inventarios.dao.TransaccionDAOImpl;
@Service("TransaccionService")
public class TransaccionServiceImpl implements TransaccionService {
	@Autowired
	private TransaccionDAOImpl transaccionDAO;
	@Override
	public List<TransaccionBean> listarTransacciones(String tipo) throws Exception {
		
		return transaccionDAO.selTransacciones(tipo);
	}

}
