package pe.edu.idat.inventarios.service;

import java.util.ArrayList;
import java.util.Map;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.inventarios.bean.GuiaCabeceraBean;
import pe.edu.idat.inventarios.bean.GuiaDetalleBean;
import pe.edu.idat.inventarios.dao.GuiaCabeceraDAOImpl;
import pe.edu.idat.inventarios.dao.GuiaDetalleDAOImpl;


@Service("GuiaCabeceraService")
public class GuiaCabeceraServiceImpl implements GuiaCabeceraService{

	// DECLARACIONES SERVICE
	//private static final Log log = LogFactory.getLog(UsuarioServiceImp.class);
	final static Logger log = Logger.getLogger(GuiaCabeceraServiceImpl.class);

	GuiaCabeceraBean guiaCabeceraBean;	
	private boolean result;
	
	@Autowired
	private GuiaCabeceraDAOImpl guiaCabeceraDAO;
	@Autowired
	private GuiaDetalleDAOImpl guiaDetalleDAO;
	
	@Override
	public ArrayList<GuiaCabeceraBean> listarGuiaCabecera() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - GuiaCabeceraServiceImpl.listarGuias");

		ArrayList<GuiaCabeceraBean> lst = new ArrayList<>();	
		try {
			// LISTAR GUIAS
			lst = guiaCabeceraDAO.selGuiaCabecera();
						
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - GuiaCabeceraServiceImpl.listarGuias");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - GuiaCabeceraServiceImpl.listarGuias");
		} 	
		return lst;
	}

	@Override
	public GuiaCabeceraBean listarGuiaCabeceraxCorrel(String correlativo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - GuiaCabeceraServiceImpl.listarGuias");
		try {
			// LISTAR GUIA
			guiaCabeceraBean = guiaCabeceraDAO.selGuiaCabeceraByCorrel(correlativo);
			// LISTAR DETALLE GUIA
			ArrayList<GuiaDetalleBean> lstDetalle = guiaDetalleDAO.selGuiaDetalleByCorrel(correlativo);
			if(lstDetalle != null){
				guiaCabeceraBean.setGic_detalle(lstDetalle);
			}						
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - GuiaCabeceraServiceImpl.listarGuias");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - GuiaCabeceraServiceImpl.listarGuias");
		} 	
		return guiaCabeceraBean;
	}

	@Override
	public boolean insertarGuiaCabecera(GuiaCabeceraBean g) throws Exception {		
		if (log.isDebugEnabled()) log.debug("Inicio - GuiaCabeceraServiceImpl.insertarGuiaCabecera");
		
		try {
			//INSERTAR CABECERA 1.0
			Map<String,Object> out = guiaCabeceraDAO.insGuiaCabecera(g);
			
			if(out.get("out_correl") != null){//CORRELATIVO GENERADO CABECERA
				
				String strCorrel = out.get("out_correl").toString();
				g.setGic_correl(strCorrel);
				
				//OBTENER LAS LISTA DE ARTICULOS DE LA GUIA
				ArrayList<GuiaDetalleBean> lst = g.getGic_detalle();
				
				for(int x=0; x < lst.size(); x++) {
					//OBTENER ARTICULO
					GuiaDetalleBean item = new GuiaDetalleBean();
					//GuiaDetalleBean item = (GuiaDetalleBean) lst.get(x); //?
					@SuppressWarnings("unchecked")
					Map<String,Object> map = (Map<String,Object>) lst.get(x) ;
					
					item.setGid_correl(strCorrel);
					item.setGid_tipo(map.get("gid_tipo").toString());
					item.setGid_tracod(map.get("gid_tracod").toString());
					item.setGid_artcod(map.get("gid_artcod").toString());
					item.setGid_cantid( Double.parseDouble((map.get("gid_cantid").toString())));
					item.setGid_estcod(Integer.valueOf(map.get("gid_estcod").toString()));
					
					//INSERTAR DETALLE 2.0
					result = guiaDetalleDAO.insGuiaDetalle(item);
					if(!result) { return result;} 
				}
				result = true;
			}
			
		}catch(Exception ex) {
			result = false;
			if (log.isDebugEnabled()) log.debug("Error - GuiaCabeceraServiceImpl.insertarGuiaCabecera");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - GuiaCabeceraServiceImpl.insertarGuiaCabecera");
		} 
		
		return result;
	}


}