package pe.edu.idat.inventarios.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.inventarios.bean.ParametroBean;
import pe.edu.idat.inventarios.controller.UsuarioController;
import pe.edu.idat.inventarios.dao.ParametroDAOImpl;

@Service("ParametroService")
public class ParametroServiceImpl implements ParametroService {	
	// DECLARACIONES SERVICE
	//private static final Log log = LogFactory.getLog(UsuarioServiceImp.class);	
	final static Logger log = Logger.getLogger(UsuarioController.class);
	
	@Autowired
	private ParametroDAOImpl parametroDAO;
	
	// METODO LISTAR TODOS
	@Override
	public List<ParametroBean> listarParametros(Map<String, Object> parametros) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ParametroServiceImpl.listarParametros");
		List<ParametroBean> lst = new ArrayList<>();	
		try {
			lst = parametroDAO.selParametro(parametros);
			// POLITICA DE NEGOCIO
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - ParametroServiceImpl.listarParametros");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ParametroServiceImpl.listarParametros");
		} 	
		return lst;
	}
}
