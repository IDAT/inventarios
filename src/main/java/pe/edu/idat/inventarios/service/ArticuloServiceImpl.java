package pe.edu.idat.inventarios.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.inventarios.bean.ArticuloBean;
import pe.edu.idat.inventarios.dao.ArticuloDAOImpl;

@Service("ArticuloService")
public class ArticuloServiceImpl implements ArticuloService {
	// DECLARACIONES SERVICE
	private static final Log log = LogFactory.getLog(UsuarioServiceImp.class);
	
	@Autowired
	private ArticuloDAOImpl articuloDAO;
	@Override
	public List<ArticuloBean> listarArticulosxDescri(String c) throws Exception {	
		if (log.isDebugEnabled()) log.debug("Inicio - ArticuloServiceImpl.listarArticulosxDescri");

		List<ArticuloBean> lst = new ArrayList<>();	
		try {
			// LISTAR ARTICULOS X DESCRIPCION
			lst = articuloDAO.selArticulosByDescri(c);
						
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - ArticuloServiceImpl.listarArticulosxDescri");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ArticuloServiceImpl.listarArticulosxDescri");
		} 	
		return lst;
	}
	@Override
	public List<ArticuloBean> listarArticulos() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ArticuloServiceImpl.listarArticulos");

		List<ArticuloBean> lst = new ArrayList<>();	
		try {
			// LISTAR ARTICULOS X DESCRIPCION
			lst = articuloDAO.selArticulos();
						
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - ArticuloServiceImpl.listarArticulos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ArticuloServiceImpl.listarArticulos");
		} 	
		return lst;
	}

}
