package pe.edu.idat.inventarios.service;

import java.util.List;

import pe.edu.idat.inventarios.bean.UsuarioBean;

public interface UsuarioService {
	
	public List<UsuarioBean> listarUsuario() throws Exception;	
	public UsuarioBean listarUsuarioxCodigo(String usuCodigo) throws Exception;
	public boolean insertarUsuario(UsuarioBean u) throws Exception;
	public boolean actualizarUsuario(UsuarioBean u) throws Exception;
	public boolean eliminarUsuarioxCodigo(String usuCodigo) throws Exception;
	public UsuarioBean listarUsuarioxCodigoAndPassword(String usuCodigo,String usuPassword) throws Exception;
}
