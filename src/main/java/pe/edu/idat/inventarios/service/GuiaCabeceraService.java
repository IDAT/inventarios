package pe.edu.idat.inventarios.service;

import java.util.ArrayList;
import pe.edu.idat.inventarios.bean.GuiaCabeceraBean;

public interface GuiaCabeceraService {
	public ArrayList<GuiaCabeceraBean> listarGuiaCabecera() throws Exception;
	public GuiaCabeceraBean listarGuiaCabeceraxCorrel(String guiaCorrel) throws Exception;
	public boolean insertarGuiaCabecera(GuiaCabeceraBean g) throws Exception;
}
