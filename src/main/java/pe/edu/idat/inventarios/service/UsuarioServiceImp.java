package pe.edu.idat.inventarios.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.inventarios.bean.ParametroBean;
import pe.edu.idat.inventarios.bean.UsuarioBean;
import pe.edu.idat.inventarios.controller.UsuarioController;
import pe.edu.idat.inventarios.dao.ParametroDAOImpl;
import pe.edu.idat.inventarios.dao.UsuarioDAOImpl;
import pe.edu.idat.inventarios.util.ParametrosConstantes;

@Service("UsuarioService")
public class UsuarioServiceImp implements UsuarioService {

	// DECLARACIONES SERVICE
	//private static final Log log = LogFactory.getLog(UsuarioServiceImp.class);
	final static Logger log = Logger.getLogger(UsuarioController.class);
	private boolean result;
	UsuarioBean usuarioBean;	
	
	@Autowired
	private UsuarioDAOImpl usuarioDAO;
	@Autowired
	private ParametroDAOImpl parametroDAO;
	
	// METODO LISTAR TODOS
	@Override
	public List<UsuarioBean> listarUsuario() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioServiceImp.listarUsuario");
		List<ParametroBean> lstParametro = new ArrayList<>();
		List<UsuarioBean> lst = new ArrayList<>();	
		try {
			// LISTAR USUARIOS
			lst = usuarioDAO.selUsuario();
			
			// LISTAR ESTADOS
			Map<String, Object> mapParametro = new HashMap<>();
			mapParametro.put("codigo", ParametrosConstantes.PARAMETRO_ESTADOS_USUARIOS);
			mapParametro.put("argumento", ParametrosConstantes.PARAMETRO_DETALLE);
			
			lstParametro = parametroDAO.selParametro(mapParametro);
			
			//CARGAR LISTA DE VALOR A MAP
			Map<String, Object> mapData = new HashMap<>();
			for (ParametroBean parametroBean: lstParametro) {				
				mapData.put(parametroBean.getPar_argume().trim(), parametroBean.getPar_funcion().trim());				
			}
			if(!mapData.isEmpty()) {						
			// CARGAR DESCRIPCION DE ESTADO
				for (UsuarioBean item : lst) {
					item.setUsu_estdes(mapData.get(String.valueOf(item.getUsu_estcod())).toString());
				}		
			}
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.listarUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.listarUsuario");
		} 	
		return lst;
	}
	
	// METODO LISTAR X CODIGO
	@Override
	public UsuarioBean listarUsuarioxCodigo(String usuCodigo) throws Exception {
		try {
			usuarioBean = usuarioDAO.selUsuarioByCodigo(usuCodigo);
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.listarUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.listarUsuario");
		} 	
		return usuarioBean;
	}
	
	// METODO INSERTAR
	@Override
	public boolean insertarUsuario(UsuarioBean u) throws Exception {
		try {
			//POLITICA DEL SISTEMA
			u.setUsu_fecreg(new Date());
			
			result = usuarioDAO.insUsuario(u);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.insertarUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.insertarUsuario");
		} 	
		return result;
	}
	
	// METODO ACTUALIZAR
	@Override
	public boolean actualizarUsuario(UsuarioBean u) throws Exception { 	
		try {
			result = usuarioDAO.updUsuario(u);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.actualizarUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.actualizarUsuario");
		} 	
		return result;
	}
	
	// METODO ELIMINAR X CODIGO
	@Override
	public boolean eliminarUsuarioxCodigo(String usuCodigo) throws Exception {
		try {
			result = usuarioDAO.delUsuario(usuCodigo);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.eliminarUsuarioxCodigo");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.eliminarUsuarioxCodigo");
		} 	
		return result;
	}
	
	// METODO LOGIN
	@Override
	public UsuarioBean listarUsuarioxCodigoAndPassword(String usuNombre, String usuPassword) throws Exception {		
		try {
			usuarioBean = usuarioDAO.selUsuarioByCodigoAndPassword(usuNombre, usuPassword);			
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.listarUsuarioxCodigoxPassword");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.listarUsuarioxCodigoxPassword");
		} 	
		return usuarioBean;
	}
}