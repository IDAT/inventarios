package pe.edu.idat.inventarios.model;

public abstract class EntidadModel {
	String entCodigo;
	String entDescri;
	int entEstCod;
	
	public String getEntCodigo() {
		return entCodigo;
	}
	public void setEntCodigo(String entCodigo) {
		this.entCodigo = entCodigo;
	}
	public String getEntDescri() {
		return entDescri;
	}
	public void setEntDescri(String entDescri) {
		this.entDescri = entDescri;
	}
	public int getEntEstCod() {
		return entEstCod;
	}
	public void setEntEstCod(int entEstCod) {
		this.entEstCod = entEstCod;
	}
	
}