package pe.edu.idat.inventarios.model;

import java.util.Date;

public abstract class GuiaDetalleModel {
	String gid_correl;
	Date gid_fecreg;
	String gid_tipo;
	String gid_tracod;
	String gid_artcod;
	String gid_undcod;
	double gid_cantid;
	int gid_estcod;
	
	public String getGid_correl() {
		return gid_correl;
	}
	public void setGid_correl(String gid_correl) {
		this.gid_correl = gid_correl;
	}
	public Date getGid_fecreg() {
		return gid_fecreg;
	}
	public void setGid_fecreg(Date gid_fecreg) {
		this.gid_fecreg = gid_fecreg;
	}
	public String getGid_tipo() {
		return gid_tipo;
	}
	public void setGid_tipo(String gid_tipo) {
		this.gid_tipo = gid_tipo;
	}
	public String getGid_tracod() {
		return gid_tracod;
	}
	public void setGid_tracod(String gid_tracod) {
		this.gid_tracod = gid_tracod;
	}
	public String getGid_artcod() {
		return gid_artcod;
	}
	public void setGid_artcod(String gid_artcod) {
		this.gid_artcod = gid_artcod;
	}
	
	public String getGid_undcod() {
		return gid_undcod;
	}
	public void setGid_undcod(String gid_undcod) {
		this.gid_undcod = gid_undcod;
	}
	public double getGid_cantid() {
		return gid_cantid;
	}
	public void setGid_cantid(double gid_cantid) {
		this.gid_cantid = gid_cantid;
	}
	public int getGid_estcod() {
		return gid_estcod;
	}
	public void setGid_estcod(int gid_estcod) {
		this.gid_estcod = gid_estcod;
	}

	
}
