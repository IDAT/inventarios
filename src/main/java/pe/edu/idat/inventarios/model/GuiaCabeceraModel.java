package pe.edu.idat.inventarios.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public abstract class GuiaCabeceraModel {
	String gic_correl;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT-5")
	Date gic_fecreg;
	String gic_tipo;
	String gic_tracod;
	String gic_observ;
	String gic_usucod;
	int gic_estcod;
	
	
	public String getGic_correl() {
		return gic_correl;
	}
	public void setGic_correl(String gic_correl) {
		this.gic_correl = gic_correl;
	}
	public Date getGic_fecreg() {
		return gic_fecreg;
	}
	public void setGic_fecreg(Date gic_fecreg) {
		this.gic_fecreg = gic_fecreg;
	}
	public String getGic_tipo() {
		return gic_tipo;
	}
	public void setGic_tipo(String gic_tipo) {
		this.gic_tipo = gic_tipo;
	}
	public String getGic_tracod() {
		return gic_tracod;
	}
	public void setGic_tracod(String gic_tracod) {
		this.gic_tracod = gic_tracod;
	}
	public String getGic_observ() {
		return gic_observ;
	}
	public void setGic_observ(String gic_observ) {
		this.gic_observ = gic_observ;
	}
	public String getGic_usucod() {
		return gic_usucod;
	}
	public void setGic_usucod(String gic_usucod) {
		this.gic_usucod = gic_usucod;
	}
	public int getGic_estcod() {
		return gic_estcod;
	}
	public void setGic_estcod(int gic_estcod) {
		this.gic_estcod = gic_estcod;
	}
	
}
