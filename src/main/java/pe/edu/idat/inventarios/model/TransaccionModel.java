package pe.edu.idat.inventarios.model;

import java.util.Date;

public abstract class TransaccionModel {
	String tra_codigo;
	String tra_tipo; 
	String tra_descri;	
	Date tra_fecreg;
	int tra_estcod;
	
	public String getTra_codigo() {
		return tra_codigo;
	}
	public void setTra_codigo(String tra_codigo) {
		this.tra_codigo = tra_codigo;
	}
	public String getTra_tipo() {
		return tra_tipo;
	}
	public void setTra_tipo(String tra_tipo) {
		this.tra_tipo = tra_tipo;
	}
	public String getTra_descri() {
		return tra_descri;
	}
	public void setTra_descri(String tra_descri) {
		this.tra_descri = tra_descri;
	}
	public Date getTra_fecreg() {
		return tra_fecreg;
	}
	public void setTra_fecreg(Date tra_fecreg) {
		this.tra_fecreg = tra_fecreg;
	}
	public int getTra_estcod() {
		return tra_estcod;
	}
	public void setTra_estcod(int tra_estcod) {
		this.tra_estcod = tra_estcod;
	}
	
	

}
