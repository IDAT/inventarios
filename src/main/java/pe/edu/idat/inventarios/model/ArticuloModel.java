package pe.edu.idat.inventarios.model;

import java.util.Date;

public abstract class ArticuloModel {
	
	String art_codigo;	
	String art_lincod;	
	String art_catcod;	
	String art_descri;	
	String art_undcod;	
	Double art_stomin;	
	Double art_stomax;	
	String art_usucod;	
	Date art_fecreg;	
	int art_estcod;
	
	public String getArt_codigo() {
		return art_codigo;
	}
	public void setArt_codigo(String art_codigo) {
		this.art_codigo = art_codigo;
	}
	public String getArt_lincod() {
		return art_lincod;
	}
	public void setArt_lincod(String art_lincod) {
		this.art_lincod = art_lincod;
	}
	public String getArt_catcod() {
		return art_catcod;
	}
	public void setArt_catcod(String art_catcod) {
		this.art_catcod = art_catcod;
	}
	public String getArt_descri() {
		return art_descri;
	}
	public void setArt_descri(String art_descri) {
		this.art_descri = art_descri;
	}
	public String getArt_undcod() {
		return art_undcod;
	}
	public void setArt_undcod(String art_undcod) {
		this.art_undcod = art_undcod;
	}
	public Double getArt_stomin() {
		return art_stomin;
	}
	public void setArt_stomin(Double art_stomin) {
		this.art_stomin = art_stomin;
	}
	public Double getArt_stomax() {
		return art_stomax;
	}
	public void setArt_stomax(Double art_stomax) {
		this.art_stomax = art_stomax;
	}
	public String getArt_usucod() {
		return art_usucod;
	}
	public void setArt_usucod(String art_usucod) {
		this.art_usucod = art_usucod;
	}
	public Date getArt_fecreg() {
		return art_fecreg;
	}
	public void setArt_fecreg(Date art_fecreg) {
		this.art_fecreg = art_fecreg;
	}
	public int getArt_estcod() {
		return art_estcod;
	}
	public void setArt_estcod(int art_estcod) {
		this.art_estcod = art_estcod;
	}
	
	

}
