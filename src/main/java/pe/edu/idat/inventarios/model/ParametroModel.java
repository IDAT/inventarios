package pe.edu.idat.inventarios.model;

import java.util.Date;

public abstract class ParametroModel{		
	String par_codigo;
	String par_tipcod;
	String par_argume;
	String par_funcion;
	String par_usucod;
	Date par_fecreg;
	
	public String getPar_codigo() {
		return par_codigo;
	}
	public void setPar_codigo(String par_codigo) {
		this.par_codigo = par_codigo;
	}
	public String getPar_tipcod() {
		return par_tipcod;
	}
	public void setPar_tipcod(String par_tipcod) {
		this.par_tipcod = par_tipcod;
	}
	public String getPar_argume() {
		return par_argume;
	}
	public void setPar_argume(String par_argume) {
		this.par_argume = par_argume;
	}
	public String getPar_funcion() {
		return par_funcion;
	}
	public void setPar_funcion(String par_funcion) {
		this.par_funcion = par_funcion;
	}
	public String getPar_usucod() {
		return par_usucod;
	}
	public void setPar_usucod(String par_usucod) {
		this.par_usucod = par_usucod;
	}
	public Date getPar_fecreg() {
		return par_fecreg;
	}
	public void setPar_fecreg(Date par_fecreg) {
		this.par_fecreg = par_fecreg;
	}
	
	
}
