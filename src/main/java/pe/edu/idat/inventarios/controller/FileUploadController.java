package pe.edu.idat.inventarios.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import pe.edu.idat.inventarios.util.Constantes;

@Controller
public class FileUploadController {
	
	final static Logger log = Logger.getLogger(UsuarioController.class);
	
	
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ModelAndView uploadFileHandler(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{	
		
		ModelAndView modelo = new ModelAndView();
		MultipartHttpServletRequest multipartRequest = null;
		MultipartFile multipartFile = null;
		File newFile = null;
		String fileLoc = null;

		String nombreArchivoTempZip = null;
		long tamanoPermitidoZIP = 0;

		//int longitudnombrearchivo = 0;

		try {
			if (log.isDebugEnabled()) log.debug("Procesa - NotificacionManualController.validarArchivo");						
			multipartRequest = (MultipartHttpServletRequest) request; 
			multipartFile = multipartRequest.getFile("filUploadServer");
			//--------------------------------------------------------------------------------------
			
			if (multipartFile.getSize() < tamanoPermitidoZIP ) { //|| tamanoPermitidoZIP == 0
				modelo.addObject("A01ArchivoTamanoPermitido", true);
			} else {
				modelo.addObject("A01ArchivoTamanoPermitido", false);
				return modelo;
			}
			//--------------------------------------------------------------------------------------
			
			//TODO LONGITUD NOMBRE DE ARCHIVO			
			String nombreArchivo = multipartFile.getOriginalFilename();
			//String[] listaSeparada = nombreArchivo.split("\\.");
			//Integer num = listaSeparada.length;
			//String extensionArchivo = listaSeparada[num - 1].toLowerCase();
			//--------------------------------------------------------------------------------------
			
			//TODO CREACION EN DIRECTORIO TEMPORAL
			Date fecSistema = new Date();			
			SimpleDateFormat formatoFechaExporta = new SimpleDateFormat(Constantes.FECHA_TEMP_ARCHIVO);			
			String fecVista = formatoFechaExporta.format(fecSistema);
			nombreArchivoTempZip = fecVista + "-" + nombreArchivo;			
			fileLoc  = Constantes.RUTA_SERVIDOR_ARCHIVO + nombreArchivoTempZip;				
			newFile = new File(fileLoc);
            if (!newFile.getParentFile().exists()) {//TODO - CREA DIRECTORIO SI NO EXISTE
              newFile.getParentFile().mkdirs();  
			}			
            FileCopyUtils.copy(multipartFile.getBytes(), newFile);
		}catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			
		}
            return modelo;
	}


}
