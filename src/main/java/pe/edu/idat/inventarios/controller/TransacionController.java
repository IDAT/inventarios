package pe.edu.idat.inventarios.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.edu.idat.inventarios.bean.TransaccionBean;
import pe.edu.idat.inventarios.service.TransaccionService;
import pe.edu.idat.inventarios.util.AvisosConstantes;
import pe.edu.idat.inventarios.util.Constantes;

@Controller
@RequestMapping({"Transaccion", "transaccion" })
public class TransacionController {
	final static Logger log = Logger.getLogger(UsuarioController.class);
	
	@Autowired		
	TransaccionService transaccionService;
	
	// METODO GET MOSTRAR TODOS LOS REGISTROS EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest/{tipo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public  @ResponseBody Map<String, Object> getTransacionAll(@PathVariable String tipo) throws Exception{
		log.info("UsuarioController.getTransacionAll - Iniciando.");
		Map<String, Object> mapaResult = new HashMap<>();
		try {
							
			// LISTAR USUARIOS 		
			List<TransaccionBean> lstTransacciones =transaccionService.listarTransacciones(tipo);
			
			mapaResult.put(Constantes.STATUS_TEXT, true);
			mapaResult.put(Constantes.MESSAGE_TEXT,  AvisosConstantes.AVISO_CRUD_READ.replace("{0}", String.valueOf(lstTransacciones.size())));
			mapaResult.put("data", lstTransacciones);
			
		} catch (Exception e) {			
			log.error("UsuarioController.getTransacionAll - Error");
			log.error(e.getMessage());			
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, e.getLocalizedMessage());
		}
		log.info("UsuarioController.getTransacionAll - Finalizando");
		return mapaResult;
	}
}
