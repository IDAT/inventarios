package pe.edu.idat.inventarios.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;

import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.sojo.interchange.json.JsonSerializer;
import pe.edu.idat.inventarios.bean.ParametroBean;
import pe.edu.idat.inventarios.bean.UsuarioBean;
import pe.edu.idat.inventarios.service.ParametroService;
import pe.edu.idat.inventarios.service.UsuarioService;
import pe.edu.idat.inventarios.util.AvisosConstantes;
import pe.edu.idat.inventarios.util.Constantes;
import pe.edu.idat.inventarios.util.ParametrosConstantes;

@Controller
@RequestMapping({"Usuario", "User" })
public class UsuarioController {
	// static Log log = LogFactory.getLog(UsuarioController.class.getName());
	final static Logger log = Logger.getLogger(UsuarioController.class);
	
	@Autowired		
	UsuarioService usuarioService;
	@Autowired		
	ParametroService parametroService;
	boolean result;
	
	// METODO GET MOSTRAR WEB -> MODALANDVIEW
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		log.info("UsuarioController.showView - Iniciando.");
		ModelAndView modelo = new ModelAndView();
		
		HttpSession session = request.getSession(true);
		
		try {
			// CONTROL DE SESSION DE USUARIO
			if (session == null || session.getAttribute("pUsuarioBean") == null) {				
				modelo.setViewName("../index");
				return modelo;
			}else{
				
				// MODALANDVIEW JSP
				modelo.setViewName("viewUsuarios");				
				UsuarioBean u = (UsuarioBean)session.getAttribute("pUsuarioBean");	
				modelo.addObject("sessionNombre", u.getUsu_descri());
			}
			
			
			// LISTAR USUARIOS 		
			List<UsuarioBean> lstUsuarios =usuarioService.listarUsuario();
			
			// LISTAR ESTADOS
			Map<String, Object> mapParametro = new HashMap<>();
			mapParametro.put("codigo", ParametrosConstantes.PARAMETRO_ESTADOS_USUARIOS);
			mapParametro.put("argumento", ParametrosConstantes.PARAMETRO_DETALLE);
			
			List<ParametroBean> lstEstados =parametroService.listarParametros(mapParametro);
			
			// AGREGAR LISTAS AL MODALANDVIEW
			modelo.addObject("listadoUsuarios", lstUsuarios);
			modelo.addObject("listadoEstados", new JsonSerializer().serialize(lstEstados));	//SERIALIZANDO	
		} catch (Exception e) {
			log.error("UsuarioController.showView - Error");
			log.error(e.getMessage());
			// CONTROL ERROR 500
			modelo.setViewName("view500");
			modelo.addObject(Constantes.MESSAGE_TEXT,e.getLocalizedMessage());
		}
		log.info("UsuarioController.showView - Finalizando");
		return modelo;
	}
	
	// METODO GET MOSTRAR TODOS LOS REGISTROS EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest", method = RequestMethod.GET, 
			//produces = MediaType.APPLICATION_JSON_VALUE, 
			headers = "Accept=application/json")
	public  @ResponseBody Map<String, Object> getUsuarioAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Map<String, Object> mapaResult = new HashMap<>();
		try {
							
			// LISTAR USUARIOS 		
			List<UsuarioBean> lstUsuarios =usuarioService.listarUsuario();
			
			mapaResult.put(Constantes.STATUS_TEXT, true);
			mapaResult.put(Constantes.MESSAGE_TEXT,  AvisosConstantes.AVISO_CRUD_READ.replace("{0}", String.valueOf(lstUsuarios.size())));
			mapaResult.put("data", lstUsuarios);
			
		} catch (Exception ex) {			
			ex.printStackTrace();			
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}		
		return mapaResult;
	}
	
	// METODO GET MOSTRAR UN REGISTRO EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest/{codigo}", 
			method = RequestMethod.GET, 
			//produces={"application/json","application/xml"}
			//produces = MediaType.APPLICATION_JSON_VALUE, 
			headers = "Accept=application/json"
			)
	public @ResponseBody Map<String, Object> getUsuario(@PathVariable String codigo) throws Exception  {	
		Map<String, Object> mapaResult = new HashMap<>();			 
		try {
			// LISTAR X CODIGO		
			UsuarioBean usuario =usuarioService.listarUsuarioxCodigo(codigo);
			mapaResult.put(Constantes.STATUS_TEXT, (usuario != null? true : false));
			mapaResult.put(Constantes.MESSAGE_TEXT, AvisosConstantes.AVISO_CRUD_READ.replace("{0}",(usuario != null? "1":"0")));
							
			if(usuario != null) {
				mapaResult.put("data", usuario);
			}
			
		} catch (Exception ex) {			
			ex.printStackTrace();
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}			
		return mapaResult;
	}
	
	// METODO POST INSERTAR UN REGISTRO  <- JSON
	@RequestMapping(value="/Rest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public @ResponseBody Map<String, Object> postUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> mapaResult = new HashMap<>();		
		try {
					
			//LECTURA DEL REQUEST BODY JSON
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"UTF-8"));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        		jsonEnviado.append(line).append('\n');
	        }
	        //----------------------------------------------
	        // SERIALIZAMOS EL JSON -> CLASS 
			UsuarioBean usuarioNuevo = (UsuarioBean) new JsonSerializer().deserialize(jsonEnviado.toString(), UsuarioBean.class);			
			//----------------------------------------------
			// SERVICE INSERTAR USUARIO
			result = usuarioService.insertarUsuario(usuarioNuevo);
			//----------------------------------------------
			mapaResult.put(Constantes.STATUS_TEXT, result);		
			mapaResult.put(Constantes.MESSAGE_TEXT, AvisosConstantes.AVISO_CRUD_INSERT.replace("{0}", (result ? "1": "0")));
		} catch (Exception ex) {
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}			
		return mapaResult;
	}
	
	// METODO PUT ACTUALIZAR UN REGISTRO  <- JSON
		@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public @ResponseBody Map<String, Object> putUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		Map<String, Object> mapaResult = new HashMap<>();		
		try {		
			//LECTURA DEL REQUEST BODY
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"UTF-8"));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
	        
			UsuarioBean usuarioNuevo = (UsuarioBean) new JsonSerializer().deserialize(jsonEnviado.toString(), UsuarioBean.class);			
			
			// ACTUALIZAR USUARIO
			result = usuarioService.actualizarUsuario(usuarioNuevo);
			mapaResult.put(Constantes.STATUS_TEXT, result);
			mapaResult.put(Constantes.MESSAGE_TEXT, AvisosConstantes.AVISO_CRUD_UPDATE.replace("{0}", (result ? "1": "0")));			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}			
		return mapaResult;
	}
	
	// METODO DELETE ELIMINAR UN REGISTRO  <- PARAMETER
	@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.DELETE, 
			//produces = MediaType.APPLICATION_JSON_VALUE, 
			headers = "Accept=application/json")
	public @ResponseBody Map<String, Object> delUsuario(@PathVariable String codigo) throws Exception {
		Map<String, Object> mapaResult = new HashMap<>();		
		try {			
			// ELIMINAR USUARIO
			result = usuarioService.eliminarUsuarioxCodigo(codigo);			
			mapaResult.put("status", result);
			mapaResult.put("message", AvisosConstantes.AVISO_CRUD_DELETE.replace("{0}", (result ? "1": "0")));
					
		} catch (Exception ex) {			
			ex.printStackTrace();			
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}		
		return mapaResult;
	}
	
	// METODO POST LOGIN USUARIO EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public @ResponseBody Map<String, Object> LogUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> mapaResult = new HashMap<String, Object>();	
		HttpSession session = request.getSession(true);
		try {					
			// LECTURA DEL REQUEST BODY JSON <- {"USU":"ATORRESCH","PWD":"P@$$w0rd"}
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"UTF-8"));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
	        //----------------------------------------------
	        //SERIALIZAMOS EL JSON -> MAP
			@SuppressWarnings("unchecked")
			Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado.toString(), Map.class);
			//----------------------------------------------
			//SERVICIO LISTAR USUARIO X CODIGO & PASSWORD		
			UsuarioBean usuario =usuarioService.listarUsuarioxCodigoAndPassword(dataEnvio.get("USU").toString(), dataEnvio.get("PWD").toString());
			//----------------------------------------------
			
			mapaResult.put(Constantes.STATUS_TEXT, (usuario != null ? true : false));
				
			if(usuario != null) {				
				session.setAttribute("pUsuarioBean", usuario);
				mapaResult.put("data", usuario);
				mapaResult.put("message", AvisosConstantes.AVISO_LOGIN_OK.replace("{0}", usuario.getUsu_descri()));
				
			}else{
				mapaResult.put("message", AvisosConstantes.AVISO_LOGIN_KO);
			}
			
		} catch (Exception ex) {			
			ex.printStackTrace();
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}	
		return mapaResult;
	}
	
	// METODO POST LOGOUT USUARIO RESPONSEBODY	
	@RequestMapping(value="/Logout", method = RequestMethod.GET)
	public ModelAndView OutUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView modelo = new ModelAndView();			
		HttpSession session = request.getSession(true);
		session.invalidate();
		
		modelo.setViewName("../index");
		return modelo;
	}
	
	
	// METODO EXPORTAR XLS
	@RequestMapping(value="/Xls", method = RequestMethod.GET)	
	public ModelAndView expUsuariosXLS(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model =  null;
		FileInputStream inputStream =  null;
		FileOutputStream fileOut = null;
		
		String[] sColumns = {"Codigo", "Nombre", "Password", "Estado"};
		int iFila = 5;
		int iColumna = 1;
		response.setContentType("application/vnd.ms-excel");		

		//CREAR LIBRO XLS
		try(Workbook workbook = new HSSFWorkbook();) {			
			// LISTAR USUARIOS 		
			List<UsuarioBean> lstUsuarios =usuarioService.listarUsuario();
			
			//CREAR HOJA XLS
			Sheet sheet = workbook.createSheet("Usuarios");
			
			//CREAR UNA FUENTE DE COLOR AZUL BOLD
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			
			// CREAR UN ESTILO TITULO			
			CellStyle titleCellStyle = workbook.createCellStyle();
			titleCellStyle.setFont(headerFont);
			
			titleCellStyle.setAlignment(HorizontalAlignment.CENTER);
			titleCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			
			
			// CREAR UN ESTILO CABECERA
			CellStyle headerCellStyle = workbook.createCellStyle();
			
			headerCellStyle.setFont(headerFont);
						
			headerCellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
			headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			    
			headerCellStyle.setBorderTop(BorderStyle.HAIR);			
			headerCellStyle.setBorderBottom(BorderStyle.THICK);
			headerCellStyle.setBorderLeft(BorderStyle.HAIR);
			headerCellStyle.setBorderRight(BorderStyle.HAIR);
			
			headerCellStyle.setBottomBorderColor(IndexedColors.CORAL.getIndex());
			headerCellStyle.setLeftBorderColor(IndexedColors.CORAL.getIndex());
			headerCellStyle.setRightBorderColor(IndexedColors.CORAL.getIndex());
			headerCellStyle.setTopBorderColor(IndexedColors.CORAL.getIndex());
			
			headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
			headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			
			
			// CREAR FILA TITULO			
			Row titleRow = sheet.createRow((short) 1);
			Cell cellTitle = titleRow.createCell((short) 1);
			cellTitle.setCellValue("LISTADO DE USUARIOS");
			cellTitle.setCellStyle(titleCellStyle);
			
			// COMBINAR FILAS + COLUMNAS
	        sheet.addMergedRegion(new CellRangeAddress(1,1,1,4));
	        
			// CREAR FILA CABECERA
			Row headerRow = sheet.createRow(iFila - 1);
	
			// CREAR CELDAS PARA A FILA
			for (int col = 0; col < sColumns.length; col++) {		
				Cell cell = headerRow.createCell(col + iColumna);
				cell.setCellValue(sColumns[col]);
				cell.setCellStyle(headerCellStyle);
			}		
			
			// CREAR LAS FILAS DETALLE
			for (UsuarioBean u : lstUsuarios) {
				Row row = sheet.createRow(iFila++);
	 
				row.createCell(0 + iColumna).setCellValue(u.getUsu_codigo());
				row.createCell(1 + iColumna).setCellValue(u.getUsu_nombre());
				row.createCell(2 + iColumna).setCellValue(u.getUsu_passwd());
	 
				Cell numberCell = row.createCell(3 + iColumna);
				numberCell.setCellValue(u.getUsu_estcod());		
			}
			
	 		// AUTOAJUSTE DE COLUMNS
			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
			sheet.autoSizeColumn(3);
			
		
			// CREATE CARPETA TEMPORALMENTE SI NO EXISTE
			File dir = new File(Constantes.CARPETA_TEMPORAL_TEMPO);
			if (!dir.exists()) { dir.mkdirs(); }
				
			// CREATE ARCHIVO TEMPORALMENTE
			File archivo = new File(Constantes.CARPETA_TEMPORAL_TEMPO + "Usuarios_" + strDateTime() + ".xls");				
			fileOut  = new FileOutputStream(archivo);	
			
			
			// GUARDA LOS CAMBIOS TEMPORALMENTE	
			workbook.write(fileOut);		
			
			
			// DOWLOAD FILE
			inputStream = new FileInputStream(archivo);
			response.setContentLength((int) archivo.length());
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", archivo.getName()));
			
			OutputStream outStream = response.getOutputStream();
			int bufferSize = 8192;			
	
			byte[] buffer = new byte[bufferSize];
			int bytesRead = -1;
	
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			
			fileOut.close();
			outStream.close();
			inputStream.close();
						
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}
	
	// METODO EXPORTAR PDF
	@RequestMapping(value="/Pdf", method = RequestMethod.GET)	
	private void expUsuariosPDF (HttpServletRequest request, HttpServletResponse response)throws IOException {        
        	response.reset();
			response.setContentType("application/pdf");
			response.setHeader("Cache-Control", "no-store");
			response.setHeader("Cache-Control", "private");
			response.setHeader("Pragma", "no-store");
			
			try {
				// LISTAR USUARIOS 		
				List<UsuarioBean> lstUsuarios =usuarioService.listarUsuario();
				
				// CONVERTIR LIST TO JRBeanCollectionDataSource 
	            JRBeanCollectionDataSource itemsJRBean = new JRBeanCollectionDataSource(lstUsuarios);
				
				String strFileName = "plantilla";
				// PARAMETROS QUE ENVIAMOS AL REPORT.			
				Map<String, Object> parameters = new HashMap<>();
				parameters.put("parametro_title", "Listado de Usuarios");
				parameters.put("parametro_author", "Alex Fernando Torres Chahuara");
				parameters.put("parametro_data", itemsJRBean);
				
				//String strSourceFileName = request.getSession().getServletContext().getRealPath("/WEB-INF/jasper/" + strFileName + ".jrxml");	
				//JasperCompileManager.compileReportToFile(strSourceFileName);

				// USANDO EL .JASPER COMPILADO PARA GENERAR EL PDF
				String strSourceFileName = request.getSession().getServletContext().getRealPath("/WEB-INF/jasper/" + strFileName + ".jasper");				
				JasperReport report = (JasperReport) JRLoader.loadObjectFromFile(strSourceFileName);								
				//JasperPrint jasperPrint = JasperFillManager.fillReport(report,parameters, itemsJRBean);				
				JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource()); 
				
				byte[] pdfReport = JasperExportManager.exportReportToPdf(jasperPrint);
					        
		        response.setContentLength(pdfReport.length);
				response.getOutputStream().write(pdfReport);
				
				response.getOutputStream().flush();
				response.getOutputStream().close();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
    }
	
	private String strDateTime() {
		
		Calendar calendar = Calendar.getInstance();

		int anio = (calendar.get(Calendar.YEAR));
		int dia = (calendar.get(Calendar.DATE));
		int hora = (calendar.get(Calendar.HOUR_OF_DAY));
		int minutos = (calendar.get(Calendar.MINUTE));
		int seconds = (calendar.get(Calendar.SECOND));
		int miliseconds = (calendar.get(Calendar.MILLISECOND));
		String dd = (dia < 10) ? "0" + dia : dia + "";

		int mes = calendar.get(Calendar.MONTH) + 1;
		String mm = (mes < 10) ? "0" + mes : mes + "";
		return anio + mm + dd + "_" + hora + minutos + seconds + miliseconds;
		
	}
	
}