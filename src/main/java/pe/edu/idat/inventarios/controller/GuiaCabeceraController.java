package pe.edu.idat.inventarios.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import net.sf.sojo.interchange.json.JsonSerializer;
import pe.edu.idat.inventarios.bean.GuiaCabeceraBean;
import pe.edu.idat.inventarios.bean.ParametroBean;
import pe.edu.idat.inventarios.bean.UsuarioBean;
import pe.edu.idat.inventarios.service.GuiaCabeceraService;
import pe.edu.idat.inventarios.service.ParametroService;
import pe.edu.idat.inventarios.util.AvisosConstantes;
import pe.edu.idat.inventarios.util.Constantes;
import pe.edu.idat.inventarios.util.ParametrosConstantes;

@Controller
@RequestMapping({"Guia", "guia" })
public class GuiaCabeceraController {
	// DECLARACIONES CONTROLLER
	@Autowired		
	GuiaCabeceraService guiaCabeceraService;
	@Autowired		
	ParametroService parametroService;
	boolean result;
	
	// METODO GET MOSTRAR WEB -> MODALANDVIEW
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelo = new ModelAndView();
		
		HttpSession session = request.getSession(true);
		
		try {
			// CONTROL DE SESSION DE USUARIO
			if (session == null || session.getAttribute("pUsuarioBean") == null) {				
				modelo.setViewName("../index");
				return modelo;
			}else{
				
				// MODALANDVIEW JSP
				modelo.setViewName("viewGuia");				
				UsuarioBean u = (UsuarioBean)session.getAttribute("pUsuarioBean");	
				modelo.addObject("sessionNombre", u.getUsu_descri());
				
				
				// LISTAR ESTADOS
				Map<String, Object> mapParametro = new HashMap<>();
				mapParametro.put("codigo", ParametrosConstantes.PARAMETRO_ESTADOS_GUIA);
				mapParametro.put("argumento", ParametrosConstantes.PARAMETRO_DETALLE);
				
				List<ParametroBean> lstEstados =parametroService.listarParametros(mapParametro);
				
				// AGREGAR LISTAS AL MODALANDVIEW
				modelo.addObject("listadoEstados", new JsonSerializer().serialize(lstEstados));	//SERIALIZANDO	
				
			}
		} catch (Exception e) {
			// CONTROL ERROR 500
			modelo.setViewName("view500");
			modelo.addObject(Constantes.MESSAGE_TEXT,e.getLocalizedMessage());
		}
		return modelo;
	}
	
	// METODO GET MOSTRAR TODOS LOS REGISTROS EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public  @ResponseBody Map<String, Object> getGuiaCabeceraAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Map<String, Object> mapaResult = new HashMap<>();
		try {
							
			// LISTAR USUARIOS 		
			List<GuiaCabeceraBean> lstUsuarios =guiaCabeceraService.listarGuiaCabecera();
			
			mapaResult.put(Constantes.STATUS_TEXT, true);
			mapaResult.put(Constantes.MESSAGE_TEXT,  AvisosConstantes.AVISO_CRUD_READ.replace("{0}", String.valueOf(lstUsuarios.size())));
			mapaResult.put("data", lstUsuarios);
			
		} catch (Exception ex) {			
			ex.printStackTrace();			
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}		
		return mapaResult;
	}
	
	// METODO GET MOSTRAR UN REGISTRO EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest/{correlativo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public @ResponseBody Map<String, Object> getGuiaCabecera(@PathVariable String correlativo) throws Exception  {	
		Map<String, Object> mapaResult = new HashMap<>();			 
		try {
			// LISTAR X CODIGO		
			GuiaCabeceraBean guiaCabeceraBean =guiaCabeceraService.listarGuiaCabeceraxCorrel(correlativo);
			mapaResult.put(Constantes.STATUS_TEXT, (guiaCabeceraBean != null? true : false));
			mapaResult.put(Constantes.MESSAGE_TEXT, AvisosConstantes.AVISO_CRUD_READ.replace("{0}",(guiaCabeceraBean != null? "1":"0")));
							
			if(guiaCabeceraBean != null) {
				mapaResult.put("data", guiaCabeceraBean);
			}
			
		} catch (Exception ex) {			
			ex.printStackTrace();
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}			
		return mapaResult;
	}
	
	
	// METODO POST INSERTAR UN REGISTRO  <- JSON
	@RequestMapping(value="/Rest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public @ResponseBody Map<String, Object> postGuiaCabecera(HttpServletRequest request, HttpServletResponse response) throws Exception {
			Map<String, Object> mapaResult = new HashMap<>();			 
		try {
			//LECTURA DEL REQUEST BODY JSON
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        		jsonEnviado.append(line).append('\n');
	        }
	        //----------------------------------------------
	        // SERIALIZAMOS EL JSON -> CLASS 
	        GuiaCabeceraBean guiaCabeceraBean = (GuiaCabeceraBean) new JsonSerializer().deserialize(jsonEnviado.toString(), GuiaCabeceraBean.class);			
			//----------------------------------------------
			// SERVICE INSERTAR USUARIO
			result = guiaCabeceraService.insertarGuiaCabecera(guiaCabeceraBean);
			//----------------------------------------------
			mapaResult.put(Constantes.STATUS_TEXT, result);		
			mapaResult.put(Constantes.MESSAGE_TEXT, AvisosConstantes.AVISO_CRUD_INSERT.replace("{0}", (result ? guiaCabeceraBean.getGic_correl(): "0")));
			
		} catch (Exception ex) {			
			ex.printStackTrace();
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}			
		return mapaResult;
	}
}
