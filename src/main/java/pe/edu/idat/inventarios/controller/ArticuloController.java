package pe.edu.idat.inventarios.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.edu.idat.inventarios.bean.ArticuloBean;
import pe.edu.idat.inventarios.service.ArticuloService;
import pe.edu.idat.inventarios.util.AvisosConstantes;
import pe.edu.idat.inventarios.util.Constantes;

@Controller
@RequestMapping({"Articulo", "articulo" })
public class ArticuloController {
	// DECLARACIONES CONTROLLER
	@Autowired		
	ArticuloService articuloService;
	
	// METODO GET MOSTRAR TODOS LOS REGISTROS EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public  @ResponseBody Map<String, Object> getUsuarioAll() throws Exception{
		Map<String, Object> mapaResult = new HashMap<>();
		List<ArticuloBean> lstArticulos = null;
		
		try {				
			// LISTAR 		
			lstArticulos =articuloService.listarArticulos();
			
			mapaResult.put(Constantes.STATUS_TEXT, true);
			mapaResult.put(Constantes.MESSAGE_TEXT,  AvisosConstantes.AVISO_CRUD_READ.replace("{0}", String.valueOf(lstArticulos.size())));
			mapaResult.put("data", lstArticulos);
			
			
		} catch (Exception ex) {			
			ex.printStackTrace();			
			mapaResult.put(Constantes.STATUS_TEXT, false);
			mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
		}		
		return mapaResult;
	}
	
	// METODO GET MOSTRAR TODOS LOS REGISTROS X DESCRI EN JSON -> RESPONSEBODY	
		@RequestMapping(value="/Rest/{descri}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
		public  @ResponseBody List<ArticuloBean> getUsuarioByDescri(@PathVariable String descri) throws Exception{
			//Map<String, Object> mapaResult = new HashMap<>();
			List<ArticuloBean> lstArticulos = null;
			
			try {
								
				// LISTAR 		
				lstArticulos =articuloService.listarArticulosxDescri(descri);
				/*
				mapaResult.put(Constantes.STATUS_TEXT, true);
				mapaResult.put(Constantes.MESSAGE_TEXT,  AvisosConstantes.AVISO_CRUD_READ.replace("{0}", String.valueOf(lstArticulos.size())));
				mapaResult.put("data", lstArticulos);
				*/
				
			} catch (Exception ex) {			
				ex.printStackTrace();			
				//mapaResult.put(Constantes.STATUS_TEXT, false);
				//mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
			}		
			return lstArticulos;
		}
	
}
