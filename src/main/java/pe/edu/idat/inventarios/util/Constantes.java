package pe.edu.idat.inventarios.util;

public class Constantes {
	private Constantes() {
		 throw new IllegalStateException("Constantes class");
	}
	public static final String STATUS_TEXT= "status";
	public static final String MESSAGE_TEXT = "message";
	
	public static final String RUTA_SERVIDOR_ARCHIVO = "/data0/uploads/spring/";
	public static final String CARPETA_TEMPORAL_UPLOADS = "/data0/uploads/";
	public static final String CARPETA_TEMPORAL_TEMPO = "/data0/tempo/";
	public static final String FECHA_TEMP_ARCHIVO = "yyyy-MM-dd_hhmmssSSS";
}
