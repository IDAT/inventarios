package pe.edu.idat.inventarios.util;

public class AvisosConstantes {
	private AvisosConstantes() {
		 throw new IllegalStateException("AvisosConstantes class");
	}
	
	public static final String AVISO_LOGIN_KO = "Usuario o Password errados";
	public static final String AVISO_LOGIN_OK = "Bienvenido {0}";
	
	public static final String AVISO_CRUD_INSERT = "{0} REGISTRO INGRESADO";
	public static final String AVISO_CRUD_READ = "{0} REGISTRO(S) ENCONTRADO(S)";
	public static final String AVISO_CRUD_UPDATE = "{0} REGISTRO ACTUALIZADO";
	public static final String AVISO_CRUD_DELETE = "{0} REGISTRO ELIMINADO";
}
