package pe.edu.idat.inventarios.util;

public class ParametrosConstantes {
	private ParametrosConstantes() {
		 throw new IllegalStateException("ParametrosConstantes class");
	}
	public static final String PARAMETRO_CABECERA = "C";
	public static final String PARAMETRO_DETALLE = "D";
	
	public static final String PARAMETRO_ESTADOS_USUARIOS = "T01";
	public static final String PARAMETRO_ESTADOS_GUIA = "T99";
	
}
