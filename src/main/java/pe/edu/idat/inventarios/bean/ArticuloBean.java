package pe.edu.idat.inventarios.bean;

import java.io.Serializable;

import pe.edu.idat.inventarios.model.ArticuloModel;

public class ArticuloBean extends ArticuloModel implements Serializable{
	private static final long serialVersionUID = 1L;
	String art_unddes;
	Double art_stoact;
	
	public String getArt_unddes() {
		return art_unddes;
	}
	public void setArt_unddes(String art_unddes) {
		this.art_unddes = art_unddes;
	}
	public Double getArt_stoact() {
		return art_stoact;
	}
	public void setArt_stoact(Double art_stoact) {
		this.art_stoact = art_stoact;
	}
	
	
}
