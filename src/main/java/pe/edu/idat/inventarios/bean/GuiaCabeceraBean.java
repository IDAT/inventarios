package pe.edu.idat.inventarios.bean;

import java.util.ArrayList;
import pe.edu.idat.inventarios.model.GuiaCabeceraModel;

public class GuiaCabeceraBean extends GuiaCabeceraModel  {

	String gic_trades;
	String gic_usudes;
	String gic_estdes;
	ArrayList<GuiaDetalleBean> gic_detalle;


	public String getGic_trades() {
		return gic_trades;
	}

	public void setGic_trades(String gic_trades) {
		this.gic_trades = gic_trades;
	}
	
	public String getGic_usudes() {
		return gic_usudes;
	}

	public void setGic_usudes(String gic_usudes) {
		this.gic_usudes = gic_usudes;
	}
	
	public String getGic_estdes() {
		return gic_estdes;
	}

	public void setGic_estdes(String gic_estdes) {
		this.gic_estdes = gic_estdes;
	}

	
	public ArrayList<GuiaDetalleBean> getGic_detalle() {
		return gic_detalle;
	}

	public void setGic_detalle(ArrayList<GuiaDetalleBean> gic_detalle) {
		this.gic_detalle = gic_detalle;
	}
	
}