package pe.edu.idat.inventarios.bean;

import pe.edu.idat.inventarios.model.EntidadModel;

public class EntidadBean extends EntidadModel{
	String entEstDes;

	public String getEntEstDes() {
		return entEstDes;
	}

	public void setEntEstDes(String entEstDes) {
		this.entEstDes = entEstDes;
	}
	
}
