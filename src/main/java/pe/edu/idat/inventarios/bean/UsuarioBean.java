package pe.edu.idat.inventarios.bean;

import java.io.Serializable;

import pe.edu.idat.inventarios.model.UsuarioModel;

public class UsuarioBean extends UsuarioModel  implements Serializable{
	private static final long serialVersionUID = 1L;
	String usu_estdes;

	public String getUsu_estdes() {
		return usu_estdes;
	}

	public void setUsu_estdes(String usu_estdes) {
		this.usu_estdes = usu_estdes;
	}
		
}