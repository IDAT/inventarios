package pe.edu.idat.inventarios.bean;


import java.io.Serializable;

import pe.edu.idat.inventarios.model.GuiaDetalleModel;

public class GuiaDetalleBean extends GuiaDetalleModel implements Serializable {
	private static final long serialVersionUID = 1L;
	String gid_artdes;
	String gid_unddes;
	
	public String getGid_artdes() {
		return gid_artdes;
	}
	public void setGid_artdes(String gid_artdes) {
		this.gid_artdes = gid_artdes;
	}
	public String getGid_unddes() {
		return gid_unddes;
	}
	public void setGid_unddes(String gid_unddes) {
		this.gid_unddes = gid_unddes;
	}
	
}
